<?php

$logAuth = new WriteLog(LOG_PATH, "qrgAuthMent.log");

if($_SESSION['count'] >= 3)
{
    header('HTTP/1.0 401 Unauthorized');
    header('location:error.php?err=401');
    die();
}


?>

<head>
 <link rel="stylesheet" type="text/css" href="./css/ui-lightness/jquery-ui-1.8.12.custom.css" />
 <script type="text/javascript" src="./js/jquery-1.5.1.min.js" >//JQuery load</script>
 <script type="text/javascript" src="./js/jquery-ui-1.8.12.custom.min.js" >//JQuery UI load</script>
 <script type="text/javascript" src="js/ajax.js"></script>
 <script type="text/javascript">
    function loadLogin()
    {
        //$("#submitLogin").button();
        //$("#cancelLogin").button();
        //login window
        $("#login").dialog({
            modal: true,
            closeOnEscape: false,
            draggable: false,
            resizable: false,
            stack: false,
            open: function(event, ui) { $(".ui-dialog-titlebar-close").hide(); },
            buttons:
            {
                //submit button
                "Submit": function()
                {
                    $("#login").dialog("close");
                    
                    //set login info information is set.
                    if($("#username").val() != "" && $("#password").val() != "")
                    {
                        //alert("submit clicked");
                        $.post("authentication.php", 
                        {username: $("#username").val(), password: $("#password").val()}, 
                        function(data)
                        {
                            window.location.reload();
                        }, "html")
                        .error(function()
                        {
                            alert("error @ qjuery:authentication.php");
                            
                        });
                    }
                    else
                    {
                        $("#loginWarning").dialog("open");
                    }
                },
                "Cancel": function()
                {
                    cancelled();
                } 
            }
            });
            
    }
    
    $(function()
    {
        $("#loginWarning").dialog(
        {
            modal: true,
            resizable: false,
            buttons: 
            {
				"Ok": function() 
                {
					$( this ).dialog( "close" );
				}
			},
            close: function()
            {
                loadLogin();
            },
            autoOpen: false,
            open: function(event, ui) { $(".ui-dialog-titlebar-close").hide(); }
        });
        

            
    });
    
    function cancelled()
    {
        $.post("authentication.php", 
            {count: <?php echo ($_SESSION['count'] + 1); ?>});
        window.location.reload();
    }
 </script>
</head>
<body>
<div id="loginWarning" title="Missing Information">
 <p>Please Enter your domain\username and password</p>
</div>
<div id="login" title="Authentication">
 <form>
  <table>
   <tr><td colspan="2">Please Enter Your Domain\Username and Password</td></tr>
   <tr><td>Username:</td><td><input type="text" name="username" id="username" /></td></tr>
   <tr><td>Password:</td><td><input type="password" name="password" id="password" /></td></tr>
   <!-- <tr><td colspan="2"><input type="button" id="submitLogin" value="Submit" /><input type="button" id="cancelLogin" value="cancel" /></td></tr> -->
  </table>
 </form>
</div>
<script type="text/javascript">
    
    loadLogin();
    
</script>
</body>

<?php
    unset($logAuth);
?>