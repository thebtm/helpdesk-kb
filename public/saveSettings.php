<?php
session_start();
/**
 * @author Brad Mouck
 * @copyright 2011
 */
require_once ("../private/initialize.php");

$log = new WriteLog(LOG_PATH, "userSaveSettings.log");

if($_SESSION['memberID'] != $_POST['memberID'])
{
    $log->write("modifying another user");
    if($_SESSION['auth'] < QR_ADMIN)
    {
        $log->write("not proper access, killing.");
        unset($log);
        header("./");
        die();
    }
}

foreach($_POST as $f => $v)
{
    $log->write("$f: $v");
    //echo "$f: $v <br />";
}
$memberID = $_POST['memberID'];
$pageBackground = $_POST['pageColor'];
$titleColor = $_POST['titleColor'];
$linkSystem = $_POST['rbMenu'];
$menuColor = $_POST['menuColor'];
$mouseOver = $_POST['mouseOver'];
$JQuery = $_POST['selectJQuery'];

$sqlColor = "call setColorSettings($memberID, '$pageBackground', '$titleColor', '$linkSystem', '$menuColor', '$mouseOver', '$JQuery')";
$mysqli->query($sqlColor);
if($mysqli->error)
{
    $log->write("error: " . $mysqli->errno . " - " . $mysqli->error);
}
include("settings.php");
?>