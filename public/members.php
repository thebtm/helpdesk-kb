<?php
session_start();
/**
 * @author Brad Mouck
 * @copyright 2011
 */
error_reporting(E_ALL ^ E_NOTICE);

//echo "is null: " . is_null($log) . "<br />";
//echo "is object: " . is_object($log) . "<br />";
//echo "is set: " . isset($log) . "<br />";

if(is_null($log))
{
    require_once("../private/initialize.php");
    $log = new WriteLog(LOG_PATH, "qrgmembers.log");
}
else
{
    $log->changeFile("qrgmembers.log");
    $log->write("called from another php page");
}

$log->write($_SESSION['auth'] < QR_ADMIN);
$log->write($_SESSION['auth']);
$log->write(QR_ACCESS);

if($_SESSION['auth'] < QR_ADMIN )
{
    header("location:./");
}





$sqlCall = "Call get_members_access()";
$sqlAccess = "call get_access_levels()";

$log->write("SQL Call: " . $sqlAccess);
$access = $mysqli->query($sqlAccess, MYSQLI_STORE_RESULT);
if($mysqli->error)
{
    $log->write("\$access mysqli error: " . $mysqli->error);
    die("\$access Error: " . $mysqli->error);
}

$i = 0;

while($accessRow = $access->fetch_array(MYSQLI_ASSOC))
{
    foreach($accessRow as $f => $v)
    {
        $accessList[$i][$f] = $v;
        $log->write("i: $i, f: $f, v: $v");
    }    
    $i++;
}
$access->free_result();
$access = null;

//need to learn more about the mysqli Class, what do these do...
while($mysqli->more_results())
{
    $mysqli->next_result();
    if($l_result = $mysqli->store_result())
    {
        $l_result->free();
    }
}


//call from members table
$log->write("SQL Call: " . $sqlCall);
$result = $mysqli->query($sqlCall, MYSQLI_STORE_RESULT);
if($mysqli->error)
{
    $log->write("\$result mysqli error: " . $mysqli->error);
    die("\$result Error: " . $mysqli->error);
}
//usleep(500);
//call from siteaccess table


$pageData = "<form id='memberupdate'><table class='members'>";
while($row = $result->fetch_array(MYSQLI_ASSOC))
{
    $pageData .= "<tr>";
    $pageData .= "<td>Username: </td><td>{$row['Username']}</td>";
    $pageData .= "<td>Last Login:</td><td>{$row['LastSignIn']}</td>";
    if($_SESSION['auth'] >= $row['sa_id'] && $_SESSION['username'] != $row['Username'] && $row['Username'] != ROOT)
    {
        $pageData .= "<td>Site Access: </td><td>";
        $pageData .= "<input type='hidden' name='id{$row['m_id']}' id='id{$row['m_id']}' value='{$row['m_id']}' />";
        $pageData .= "<input type='hidden' name='update{$row['m_id']}' id='update{$row['m_id']}' value='no' />";
        $pageData .= "<select class='access' name='siteaccess{$row['m_id']}' id='{$row['m_id']}'>";
        for($j = 0; $j < count($accessList); $j++)
        {
            if($accessList[$j]['ID'] <= $_SESSION['auth'] && $accessList[$j]['ID'] != ROOT_ACCESS)
            {
                $log->write("$j - " . $accessList[$j]['ID'] . " - " . $row['sa_id']);
                $pageData .= "<option value='" . $accessList[$j]['ID'] . "'";
                if($accessList[$j]['ID'] == $row['sa_id'])
                {
                    $pageData .= " Selected='selected' ";
                }
                $pageData .= ">{$accessList[$j]['AccessLevel']}</option>\n";
            }
            
        }
        $pageData .= "</select></td>\n";
        $pageData .= "<td><a href='javascript:getPage(\"settings.php\",\"dataPage\",\"mid={$row['m_id']}\")' >settings</a>";
    }
    else
    {
        $pageData .= "<td>Site Access: </td><td>{$row['AccessLevel']}</td><td></td>";
    }
    
    foreach($row as $field => $value)
    {
        $log->write("$field: $value");
    }
    $pageData .= "</tr>";
   
}
$pageData .= "<tr><td colspan='4'></td><td colspan='2'><input id='submitMembers' type='button' value='submit' />
              <input id='cancelMembers' type='button' value='cancel' /></td></tr>";
$pageData .= "</table></form>\n";

$pageData .= "<script type='text/javascript' >
                $(\"#submitMembers\").button();
                $(\"#submitMembers\").click(function()
                {
                    postMembers($(\"#memberupdate\"));
                });
                $(\"#cancelMembers\").button();
                $(\"#cancelMembers\").click(function()
                {
                    getMembers();
                });
                
                $(\".access\").each(function()
                {
                    $(this).change(function()
                    {
                        var item = \"#update\" + this.id;
                        $(item).val(\"yes\");
                        $(this).css('color', 'red');
                        $(this).css('font-weight', 'bold');
                    });
                });
                
             </script>";
echo $pageData;
?>

<script type="text/javascript">
    
</script>