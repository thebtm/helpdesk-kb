<?php
session_start();
/**
 * @author Brad Mouck
 * @copyright 2011
 */
error_reporting(E_ALL ^ E_NOTICE);

if(is_null($log))
{
    require_once ("../private/initialize.php");

    $log = new WriteLog(LOG_PATH, "userSettings.log");

}
else
{
    $log->changeFile("userSettings.log");
    $log->write("called from another php page");
}


    $logColor = new WriteLog(LOG_PATH, "qrUserSettings.log");
    if(!isset($memberID))
    {
        if(isset($_GET['mid']))
        {
            $memberID = $_GET['mid'];
        }
        else
        {
            $memberID = $_SESSION['memberID'];
        }
    }
    
    $sqlGetSettings = "call getColorSettings(" . $memberID . ", 1)";
    $logColor->write("sql get settings: " . $sqlGetSettings);
    $colorResult = $mysqli->query($sqlGetSettings);
    if($mysqli->error)
    {
        $logColor->write("My SQLi: " . $mysqli->error);
    }
    $logColor->write("row count: " . $mysqli->affected_rows);
    if($mysqli->affected_rows )
    {
        $colorRow = $colorResult->fetch_array(MYSQLI_ASSOC);
        foreach($colorRow as $f => $v)
        {
            $logColor->write("$f: $v");
        }
        
    }
    else
    {
        //need to learn more about the mysqli Class, what do these do...
        while($mysqli->more_results())
        {
            $mysqli->next_result();
            if($l_result = $mysqli->store_result())
            {
                $l_result->free();
            }
        }
        
        $sqlDefaultColor = "Call getDefaultColors()";
        $colorResult = $mysqli->query($sqlDefaultColor);
        if($mysqli->error)
        {
            $logColor->write($mysqli->error);
        }
        $logColor->write("row count: " . $mysqli->affected_rows);
        if($mysqli->affected_rows )
        {
            $colorRow = $colorResult->fetch_array(MYSQLI_ASSOC);
            foreach($colorRow as $f => $v)
            {
                $logColor->write("$f: $v");
            }
        }
    }
    $pageBackground = $colorRow['PageBackground'];
    $titleColor = $colorRow['TitleColor'];
    $linkSystem = $colorRow['LinkSystem'];
    $menuColor = $colorRow['MenuColor'];
    $mouseOver = $colorRow['MouseOver'];
    $JQuery = $colorRow['JQuery'];  
    
    foreach($colorRow as $f => $v)
    {
        echo("$f: $v <br />");
    }
    

    ?>
    
     <form id="userColors">
      <table>
       <tr><td>Page Color:</td><td><input class="colorwell" type="text" size="7" maxlength="7" name="pageColor" id="pageColor" value="<?php echo $pageBackground; ?>" /></td><td rowspan="5"><div id="picker" ></div> </td></tr>
       <tr><td>Team Title Color:</td><td><input class="colorwell" type="text" size="7" maxlength="7" name="titleColor" id="titleColor" value="<?php echo $titleColor; ?>" /></td></tr>
       <tr><td>Menu System:</td>
        <td>
         <input type="radio" name="rbMenu" id="rbMenuTable" value="1" <?php if($linkSystem == 1) echo "checked=\"checked\""; ?> />Table &nbsp; 
         <input type="radio" name="rbMenu" id="rbMenuDowndown" value="2" <?php if($linkSystem == 2) echo "checked=\"checked\""; ?> />Dropdown
        </td>
       </tr>
       <tr><td colspan="2">
        <div id="dDropdownOptions">
         <table>
          <tr><td>Menu Color:</td><td><input class="colorwell" type="text" size="7" maxlength="7" name="menuColor" id="menuColor" value="<?php echo $menuColor; ?>" /></td></tr>
          <tr><td>Menu Mouse Over Color:</td><td><input class="colorwell" type="text" size="7" maxlength="7" name="mouseOver" id="mouseOver" value="<?php echo $mouseOver; ?>" /></td></tr>
         </table>
        </div>
       </td></tr>
       <tr><td>Jquery Colors:<br /> current - <?php echo $JQuery; ?><br /> Dialog Window &amp; main menu</td>
        <td>
         <select name="selectJQuery" id="selectJQuery">
          <option value="ui-lightness" <?php if($JQuery == "ui-lightness") echo "selected='selected'"; ?> >ui-lightness</option>
          <option value="redmond" <?php if($JQuery == "redmond") echo "selected='selected'"; ?> >redmond</option>
          <option value="dark-hive" <?php if($JQuery == "dark-hive") echo "selected='selected'"; ?> >dark hive</option>
          <option value="excite-bike" <?php if($JQuery == "excite-bike") echo "selected='selected'"; ?> >excite bike</option>
          <option value="vader" <?php if($JQuery == "vader") echo "selected='selected'"; ?> >vader</option>
         </select>
        </td>
       </tr>
       <tr>
        <td><input type="hidden" name="memberID" value="<?php echo $memberID; ?>" /></td>
        <td><input type="hidden" name="changed" id="changed" value="0" /><input class="colorwell" type="hidden" value="#000000" /></td>
        <td><?php if($_SESSION['auth'] >= QR_ADMIN){ ?> <input type="button" value="Defaults" id="colorDefaults" /><?php } ?><input type="button" value="submit" id="colorSubmit" /><input type="button" value="cancel" id="colorCancel" /></td>
       </tr>
      </table>
     </form>
     
    <script type="text/javascript">
     var menuOption = <?php echo $linkSystem; ?>;
     
     var ckColor = new Array();
     ckColor['pageColor'] = "<?php echo $pageBackground; ?>";
     ckColor['titleColor'] = "<?php echo $titleColor; ?>";
     ckColor['menuColor'] = "<?php echo $menuColor; ?>";
     ckColor['mouseOver'] = "<?php echo $mouseOver; ?>";
     
     function displayMenuOptions(id)
     {
         //alert(id);
         if(id == <?php echo LIST_MENU; ?>)
         {
            $("#dDropdownOptions").show();
            //alert("show");
         }
         else
         {
            $("#dDropdownOptions").hide();
            //alert("hide");
         }
     }
     
     displayMenuOptions(menuOption);
     
    $(function()
    {
        $(":button").button();
        $("#colorSubmit").click(function()
        {
            //alert($("#changed").val());
            if($("#changed").val() == 1)
            {
                postPage("saveSettings.php", "dataPage", "#userColors");
            }
            else
            {
                alert("no changes where completed");
            }
         
        });
        $("#colorCancel").click(function()
        {
         getPage("settings.php", "dataPage", "mid=<?php echo $memberID; ?>");
        });
        <?php 
        if($_SESSION['auth'] >= QR_ADMIN)
        {
        ?>
        $("#colorDefaults").click(function()
        {
            $("#pageColor").val("#CAFF95");
            $("#titleColor").val("#009900");
            $("#rbMenuDowndown").attr('checked', true);
            $("#dDropdownOptions").show();
            $("#menuColor").val("#fff4bb");
            $("#mouseOver").val("#9999ff");
            $("#selectJQuery").val("ui-lightness");
            $("#changed").val("1")
            
        });
        <?php
        }
        ?>
        $("input[type='text']").each(function()
         {
             $(this).blur(function()
             {
                 if(ckColor[this.id] != this.value)
                 {
                     $("#changed").val("1");
                 }
             });
         });
         
         $("#selectJQuery").change(function()
         {
             $("#changed").val("1");
         });
        
        $("input[name='rbMenu']").each(function()
        {
            $(this).change(function()
            {
                displayMenuOptions(this.value);    
                $("#changed").val("1");
            });
        });
        
        //from the Farbtastic color picker this is GPL code
        var f = $.farbtastic('#picker');
        var p = $('#picker').css('opacity', 0.25);
        var selected;
        $('.colorwell')
        .each(function () { f.linkTo(this); $(this).css('opacity', 0.75); })
        .focus(function() 
        {
            if (selected) 
            {
                $(selected).css('opacity', 0.75).removeClass('colorwell-selected');
            }
            f.linkTo(this);
            p.css('opacity', 1);
            $(selected = this).css('opacity', 1).addClass('colorwell-selected');
        });
        //end of Farbtastic color picker
    });
    </script>
    
    <?php
?>