<?php

/**
 * @author Brad Mouck
 * @copyright 2011
 */

session_start();

require_once("../private/initialize.php");

$log = new WriteLog(LOG_PATH, "hdkb-list.php.log");

if(is_null($_GET['file']))
{
    echo getDir(LOG_PATH);
}
elseif($_GET['file'] != "")
{
    if(file_exists($_GET['file']))
    {
        $file = fopen($_GET['file'], "r");
        $contents = fread($file, filesize($_GET['file']));
        fclose($file);
        
        $contents = str_replace("\n", "<br />", $contents);
        $contents = "<br /><br /><br />file: " . $_GET['filename'] . "<br /><br />" . $contents;
    }
    else
    {
        $contents = "Error reading file";
    }
    
    echo $contents;
}
else
{
    echo " error";
}


?>