<?php
if(!isset($_SESSION))
{
    session_start();
}

/**
 * @author Brad Mouck
 * @copyright 2011
 * 
 * Links for the index.php page. all links use Jquery / AJAX to get details.
 */

if(!defined('LOADED_CONSTANTS'))
{
    require_once("../private/initialize.php");
}

$adminAccess = $_SESSION['auth'] >= QR_ADMIN;
$developAcess = $_SESSION['auth'] >= FULL_ADMIN;
$colspan = 4;
 // standard links
?>
<div id="links">
 <table class="links">
  <tr>
   <td class="links"><a href="<?php echo $_SERVER[''] ?>">Home (Group List)</a></td>
   <td class="links">report bug /<br /> suggest feedback</td>
   <?php
   if($_SESSION['auth'] < FULL_ADMIN) // something feels off about this
    {
        ?>
        <td class="links">Bug Progress Report</td>
        <?php
        $colspan = 4;
    }
    else
    {
        ?>
        <td></td>
        <?php
    }
    ?>
   <td class="links"><a href="javascript:getPage('logout.php','page');" >logout</a></td>
<?php
    

//<a href="javascript:sortGroup()">Group List</a>
// SQL statement to check for members

// admin links based on access in members database
    if($adminAccess)
    {
?>
        
        </tr>
        <tr><td class="links" colspan="<?php echo $colspan; ?>">Administation</td></tr>
        <tr>
        <td class="links"><a href="javascript:getMembers();" >Members</a></td>
        <td class="links"><a href="javascript:modifyWorkgroups(0, <?php echo NEW_ENTRY ?>, 'dTeam');">Add New Workgroup</a></td>
        <td class="links"><a href="javascript:modifyDetails(0, <?php echo NEW_ENTRY; ?>, 'dDetails');">New QRG Entry</a></td>
        <td class="links">attachment management</td>
<?php
    }
    if($developAcess)
    {
?>
    </tr>
    <tr><td class="links" colspan="<?php echo $colspan; ?>">Developer Links</td></tr>
    <tr>
    <td class="links">Tables</td>
    <td class="links"><a href="javascript:getPage('getLogs.php','dataPage')" >Error logs</a></td>
    <td class="links">bug/feedback report</td>
    <td></td>

    
<?php
    }
 ?>
  </tr>
 </table>
</div> 