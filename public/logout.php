<?php
session_start();
/**
 * @author Brad Mouck
 * @copyright 2011
 */
require_once("../private/initialize.php");

$log = new WriteLog(LOG_PATH, "logout.log");
echo "please <a href='..'>click here</a> id you would like to sign in again <br />";
foreach($_SESSION as $field => $value)
{
    $log->write("Session: $field: $value /n");
    if(is_array($value))
    {
        foreach($value as $f => $v)
        {
            $log->write("Session: $value: $f: $v /n");
        }
    }
}
echo "you have been logged out";
unset($log);
unset($_SESSION);
session_destroy();
?>