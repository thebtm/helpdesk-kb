<?php
session_start();
/**
 * @author Brad Mouck
 * @copyright 2011
 */
//error_reporting(E_ALL);
error_reporting(E_ALL ^ E_NOTICE);
require_once ("../private/initialize.php");

$log = new WriteLog(LOG_PATH, "qrSave.log");

/*

GET:
id: 5
status: 1
what: qrg 

*/
$id = $_GET['id'];
$status = $_GET['status'];
$what = $_GET['what'];
$log->write("id: " . $id . " + status: " . $status . " + what: $what");

foreach($_POST as $field => $value)
{
    if($value == "" || $value == null)
    {
        $value = null;
    }
    else
    {
        $_POST[$field] = $mysqli->real_escape_string($value);
    }
    //$sqlField .= "$field='$value',";
    $log->write("$field: $value");
}
if($what == QRG)
{
    $name = setDataForSQL($_POST['name']);
    $description = setDataForSQL($_POST['description']);
    $keywords = setDataForSQL($_POST['Keywords']);
    $selfhelp = setDataForSQL($_POST['selfhelp']);
    $tier1 = $_POST['tier1'];
    $tier2 = $_POST['tier2'];
    $tier3 = $_POST['tier3'];
    $passwordused = setDataForSQL($_POST['pass']);
    $inmultiple = setDataForSQL($_POST['incMultiple']);
    $url = setDataForSQL($_POST['url']);
    $ci = setDataForSQL($_POST['ci']);
    $incTrouble = setDataForSQL($_POST['incTrouble']);
    $ar_approvals = setDataForSQL($_POST['arApproval']);
    $ar_prerequisites = setDataForSQL($_POST['arPrereq']);
    $ar_workflow = setDataForSQL($_POST['arWorkflow']);
    $businessOwner = setDataForSQL($_POST['businessOwner']);
    $stakeholders = setDataForSQL($_POST['stakeholders']);
    $businessHours = setDataForSQL($_POST['businessHours']);
    $supportHours = setDataForSQL($_POST['supportHours']);
}
elseif($what == WG)
{
    $name = setDataForSQL($_POST['name']);
    $searchCode = setDataForSQL($_POST['searchCode']);
    $email1 = setDataForSQL($_POST['email1']);
    $email2 = setDataForSQL($_POST['email2']);
    $email3 = setDataForSQL($_POST['email3']);
    $phone = setDataForSQL($_POST['phone']);
}
if($_POST['archive'] == "on")
{
    $archived = 1;
}
else
{
    $archived = 0;
}
$modUser = setDataForSQL($modUser);
//this is for a existing 
if($what == QRG && $status == SAVE_ENTRY)
{
    $log->write("QRG & SAVE");
    /*
    $sqlSave = "Update quickreference ";
    $sqlSave .= "Set name=$name, description=$description, selfhelp=$selfhelp, tier1=$tier1, tier2=$tier2, tier3=$tier3, ";
    $sqlSave .= "passwordused=$passwordused, inmultiple=$inmultiple, url=$url, ci=$ci, incidenttroubleshooting=$incTrouble, ";
    $sqlSave .= "ar_approvals=$ar_approvals, ar_prerequisites=$ar_prerequisites, ar_workflow=$ar_workflow, ";
    $sqlSave .= "businessOwner=$businessOwner, stakeholders=$stakeholders, businessHours=$businessHours, ";
    $sqlSave .= "supportHours=$supportHours, attachment_ID=null, archived=$archived ";
    $sqlSave .= "Where id='$id'";
    */
    $sqlCall = "Call Update_qr(
                $id, 
                $name, 
                $description, 
                $keywords,
                null, 
                $selfhelp, 
                $tier1, 
                $tier2, 
                $tier3, 
                $passwordused, 
                $inmultiple,
                $url, 
                $ci, 
                $incTrouble, 
                $ar_approvals, 
                $ar_prerequisites, 
                $ar_workflow, 
                $businessOwner, 
                $stakeholders,
                $businessHours, 
                $supportHours, 
                null, 
                $archived,  
                $modUser)"; 
    
    
    $log->write("SQL Call: $sqlCall");
    $mysqli->query($sqlCall);
    if($mysqli->error)
    {
        echo $mysqli->error . "<br />";
        $log->write("MySQLi Error: " . $mysqli->error);
    }
    else
    {
        $pageData = "Record Saved.";
    }
    
    
    //$pageData = $sqlSave;
}
elseif($what == QRG && $status == NEW_ENTRY)
{
    $log->write("QRG & NEW");
    $sqlField = "";
    $sqlValue = "";
    
    $sqlCall = "Call insert_qr
        ($name, 
        $description, 
        null, 
        $keywords, 
        $selfhelp, 
        $tier1, 
        $tier2, 
        $tier3, 
        $passwordused, 
        $inmultiple, 
        $url, 
        $ci, 
        $incTrouble, 
        $ar_approvals, 
        $ar_prerequisites, 
        $ar_workflow, 
        $businessOwner, 
        $stakeholders, 
        $businessHours, 
        $supportHours, 
        null, 
        $modUser)";
        
    $result = $mysqli->query($sqlCall);
    if($mysqli->error)
    {
        $log->write($mysqli->error);
        echo $mysqli->error;
    }
    else
    {
        $saveOut = $result->fetch_array(MYSQLI_ASSOC);
    
        $pageData = "Record " . $saveOut['id'] . " Saved";
    }
    
}
elseif($what == WG && $status == NEW_ENTRY)
{
    $log->write("WG & NEW");
    //$sqlField = "";
    //$sqlValue = "";
    
    $sqlCall = "Call insert_wg($name, $searchCode, $email1, $email2, $email3, $phone, $modUser)";  
        
    $result = $mysqli->query($sqlCall);
    if($mysqli->error)
    {
        $log->write($mysqli->error);
        echo $mysqli->error;
    }
    $saveOut = $result->fetch_array(MYSQLI_ASSOC);
    foreach($saveOut as $f => $v)
    {
        $log->write("$f : $v");
    }   
    echo "New Record " . $saveOut['id'] . " Saved";
}
elseif($what == WG && $status == SAVE_ENTRY)
{
    $log->write("WG & Save");
    //$sqlField = "";
    //$sqlValue = "";
    if($archived == 1)
    {
        //check for workgroup use.
    }    
    
    $sqlCall = "Call update_wg($id, $name, $searchCode, $email1, $email2, $email3, $phone, $archived, $modUser)"; 
    $log->write($sqlCall); 
        
    $result = $mysqli->query($sqlCall);
    if($mysqli->error)
    {
        $log->write($mysqli->error);
        echo $mysqli->error;
    }
    $saveOut = $result->fetch_array(MYSQLI_ASSOC);
    
    $pageData = "Record " . $saveOut['id'] . " Saved";
}

$log->write("SQL Call: " . $sqlCall);
/*
foreach($_GET as $field => $value)
{
    $log->write("$field: $value");
    $pageData .= "$field: $value <br />\n";
}
*/


/*
$log->write("FILE:");
$pageData .= "FILE: <br />\n";
foreach($_FILE as $field => $value)
{
    $log->write("$field: $value");
    $pageData .= "$field: $value <br />\n";
}

*/
echo $pageData;
?>