<?php
session_start();
/**
 * @author Brad Mouck
 * @copyright 2011
 */

error_reporting(E_ALL ^ E_NOTICE);
require("../private/initialize.php");

$log = new WriteLog(LOG_PATH, "qrAddModifyWorkgroups.log");
$log->write("GET:");
//$pageData = "GET: <br />\n";
/*
foreach($_GET as $field => $value)
{
    $log->write("$field: $value");
    $pageData .= "$field: $value <br />\n";
}
*/
$id = $_GET['id'];
$log->write("id: " .$id);
$status =$_GET['status'];
$log->write("status: " . $status);

if($id > 0 && $status == MODIFY_ENTRY)
{
    $log->write("Modify");
    $sqlWG = "call select_wg_id($id)";
    $log->write("SqlWG: " . $sqlWG);
    
    $result = $mysqli->query($sqlWG);
    if($mysqli->error)
    {
        $log->write("MySQLi Error:" . $mysqli->error);
    }
    
    $row = $result->fetch_array(MYSQLI_ASSOC);
    foreach($row as $field => $value)
    {
        $log->write("$field: $value");
        $row[$field] = htmlspecialchars($value, ENT_QUOTES);
        //$pageData .= "$field: $value <br />\n";
    }
    
    $name = $row['Name'];
    $searchCode = $row['SearchCode'];
    $email1 = $row['EmailAddress1'];
    $email2 = $row['EmailAddress2'];
    $email3 = $row['EmailAddress3'];
    $phone = $row['OncallPhone'];
    
    $archived = "<tr><td colspan='4'>Archive <input type='checkbox' name='archive' id='archive' /></td></tr>";
    $saveAction = "javascript:saveDetails($id, " . SAVE_ENTRY . ", '" . WG . "', 'dTeam');";
    $cancelAction = "javascript:closeDiv('dTeam');";
}
elseif ($id == 0 && $status == NEW_ENTRY)
{
    $name = "";
    $searchCode = "";
    $email1 = "";
    $email2 = "";
    $email3 = "";
    $phone = "";
    $archived = "";
    
    $saveAction = "javascript:saveDetails(0, " . NEW_ENTRY . ", '" . WG . "', 'dTeam');";
    $cancelAction = "javascript:closeDiv('dTeam');";
}
else
{
    $log->write("Else: Error");
    die("an error has occured at add modify WG.");
    //die - error has occured
}

    $name = "<input type='text' class='edit1' name='name' id='name' value='$name' />";
    $searchCode = "<input class='edit1' type='searchCode' name='searchCode' id='searchCode' value='$searchCode' />";
    $email1 = "<input type='text' class='edit1' name='email1' id='email1' value='$email1' onkeypress='javascript:emailCheck()' />";
    $email2 = "<input type='text' class='edit1' name='email2' id='email2' value='$email2' onkeypress='javascript:emailCheck()' />";
    $email3 = "<input type='text' class='edit1' name='email3' id='email3' value='$email3' />";
    $phone = "<input type='text' class='edit1' name='phone' id='phone' value='$phone' />";

    $pageData .= "<form><table>";
    $pageData .= "<tr><td>Name:</td><td class='info'>$name</td><td>Search Code:</td><td class='info'>$searchCode</td></tr>";
    $pageData .= "<tr><td>Email 1:</td><td colspan='3' class='info'>$email1</td></tr>";
    $pageData .= "<tr><td>Email 2:</td><td colspan='3' class='info'>$email2</td></tr>";
    $pageData .= "<tr><td>Email 3:</td><td colspan='3' class='info'>$email3</td></tr>";
    $pageData .= "<tr><td>On Call Phone</td><td class='info' colspan='3'>$phone</td></tr>"; 
    if($_SESSION['auth'] >= QR_ADMIN)
        $pageData .= $archived;
    $pageData .= "</table></form>";
        
    $pageData .= "<input type='button' onclick=\"$saveAction\" value='Save' /> <!-- <a href=\"$saveAction\">save</a> -->";
    $pageData .= "<input type='button' onclick=\"$cancelAction\" value='Close' /> <!-- <a href='$cancelAction'>Cancal</a> -->";
        

//page data

echo $pageData;



?>