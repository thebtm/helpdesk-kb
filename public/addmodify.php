<?php
session_start();
/**
 * @author Brad Mouck
 * @copyright 2011
 */

require("../private/initialize.php");

$log = new WriteLog(LOG_PATH, "qrgAddModify.log");

$id = $_GET['id'];
$status = $_GET['status'];

if($id != "" && $status != "")
{
    $sqlTeir = "select ID, Name, Archived from workgroups order by Name";
    $wgResult = $mysqli->query($sqlTeir);
}
else
{
    exit("Missing Variable data for the required variables, please go to <a href='index.php'>home</a> page and try the site again. contact site admin.");
}

if($status == MODIFY_ENTRY && $id != 0)
{
    $sqlDetails = "Select * from quickreference where id='$id'";
    
    $result = $mysqli->query($sqlDetails);
    $row = $result->fetch_array(MYSQLI_ASSOC);
    
    foreach($row as $field => $value)
    {
        if($value == null)
        {
            $row["$field"] = "";
        }
    }
    
    $id = $row['ID'];
    $name = htmlspecialchars($row['Name']);
    $description = htmlspecialchars($row['Description']);
    $ticketDesc = htmlspecialchars($row['TicketDescription']);
    $keywords = htmlspecialchars($row['Keywords']);
    $selfhelp = htmlspecialchars($row['SelfHelp']);
    
    $pass = htmlspecialchars($row['PasswordUsed']); 
    $incMultiple = htmlspecialchars($row['INMultiple']);
    $url = htmlspecialchars($row['URL']);
    $ci = htmlspecialchars($row['CI']);
    $incTrouble = htmlspecialchars($row['IncidentTroubleshooting']);
    $arApproval = htmlspecialchars($row['AR_Approvals']);
    $arPrereq = htmlspecialchars($row['AR_Prerequisites']);
    $arWorkflow = htmlspecialchars($row['AR_Workflow']);
    $businessOwner = htmlspecialchars($row['BusinessOwner']);
    $businessHours = htmlspecialchars($row['BusinessHours']);
    $supportHours = htmlspecialchars($row['BusinessHours']);
    $stakeholders = htmlspecialchars($row['Stakeholders']);
    $attachment = $row['Attachment_ID'];
    
    $tier1 = "<option value='null'>None Selected</option>";
    $tier2 = "<option value='null'>None Selected</option>";
    $tier3 = "<option value='null'>None Selected</option>";
    
    while($wgRow = $wgResult->fetch_array(MYSQLI_ASSOC))
    {
        if($wgRow['Archived'] != 1 || $_SESSION['auth'] >= QR_ADMIN)
        {
            $tier1 .= "<option value='" . $wgRow['ID'] . "' ";
            $tier2 .= "<option value='" . $wgRow['ID'] . "' ";
            $tier3 .= "<option value='" . $wgRow['ID'] . "' ";
            
            If($row['Tier1'] == $wgRow['ID'])
            {
                $tier1 .= " selected='selected' ";
            }
            
            If($row['Tier2'] == $wgRow['ID'])
            {
                $tier2 .= " selected='selected' ";
            }
            
            If($row['Tier3'] == $wgRow['ID'])
            {
                $tier3 .= " selected='selected' ";
            }
            if($wgRow['Archived'] == 1)
            {
                $tier1 .= ">" . $wgRow['Name'] . " - Archived</option>\n";
                $tier2 .= ">" . $wgRow['Name'] . " - Archived</option>\n";
                $tier3 .= ">" . $wgRow['Name'] . " - Archived</option>\n";
            }
            else
            {
                $tier1 .= ">" . $wgRow['Name'] . "</option>\n";
                $tier2 .= ">" . $wgRow['Name'] . "</option>\n";
                $tier3 .= ">" . $wgRow['Name'] . "</option>\n";
            }
            
        }
    }
    
    $saveAction = "javascript:saveDetails($id, " . SAVE_ENTRY . ", \"" . QRG . "\", \"dDetails\")";
    $cancelAction = "javascript:showDetailsAjax($id, \"dDetails\", \"" . QRG . "\");";
    $addArchive = "Archive? <input type='checkbox' name='archive' id='archive' /> <br />";
    
}
elseif($status == NEW_ENTRY)
{
    $name = "";
    $description = "";
    
    $tier1 = "<option value='null' selected='selected'>None Selected</option>";
    
    while($wgRow = $wgResult->fetch_array(MYSQLI_ASSOC))
    {
        $tier1 .= "<option value='" . $wgRow['ID'] . "' >" . $wgRow['Name'] . "</option>\n";
    }
    
    $tier2 = $tier1;
    $tier3 = $tier1;
    
    $saveAction = "javascript:saveDetails(0, " . NEW_ENTRY . ", \"" . QRG . "\", \"dDetails\")";
    $cancelAction = "javascript:closeDiv(\"dDetails\");";
}
else
{
    die("Error has occured at trying to determine the modify/new status or this page was improperly requested.");
}

$pageData ="<form name='entry' id='entry'>";
$pageData .= "<table id='details'>\n";
$pageData .= "<tr>";
$pageData .= "<td class='field d1 required'>Name:</td><td class='info d2'><input type='text' class='edit1' name='name' id='name' maxsize='255' value='" . $name . "' /></td>";
$pageData .= "<td class='field d3'>Business Owner</td><td class='info d4'><input type='text' class='edit2' name='businessOwner' id='businessOwner' maxsize='255' value='" . $businessOwner . "' /></td>";
$pageData .= "</tr><tr>";
$pageData .= "<td class='field required'>Description:</td><td class='info' rowspan='2'><textarea class='edit1' name='description' id='description' rows='3' maxsize='4000' >" . $description . "</textarea></td>";
$pageData .= "<td class='field'>Stakeholders</td><td class='info'><input class='edit2' type='text' name='stakeholders' id='stakeholders' value='" . $stakeholders . "' /></td>";
$pageData .= "</tr><tr>";
$pageData .= "<td></td><td class='field'>Business Hours</td><td class='info'><input class='edit2' type='text' name='businessHours' id='businessHours' maxsize='255' value='" . $businessHours . "' /></td>";
$pageData .= "</tr><tr>";
$pageData .= "<td class='field'>Keywords: </td><td class='info'><input type='text' name='keywords' id='keywords' value='$keywords' /></td><td class='field'>IT Support Hours</td><td class='info'><input class='edit2' type='text' name='supportHours' id='supportHours' maxsize='255' value='" . $supportHours . "' /></td>";
$pageData .= "</tr><tr>";
$pageData .= "<td colspan='2' class='field center'>Incident (Single)</td><td class='field'>URL</td><td class='info' ><input type='text' class='edit2' name='url' id='url' maxsize='255' value='" . $url . "' /></td>";
$pageData .= "</tr><tr>";
$pageData .= "<td class='field'>Self Help:</td><td class='info'><input class='edit1' type='text' name='selfhelp' id='selfhelp' maxsize='255' value='" . $selfhelp . "' /></td><td class='field center' colspan='2'>CI</td>";
$pageData .= "</tr><tr>\n";
$pageData .= "<td class='field required'>Teir 1:</td><td class='info'><select name='tier1' id='tier1' class='tiers'>";
$pageData .= $tier1;
$pageData .= "</select>\n";
$pageData .= "</td><td class='info' colspan='2' rowspan='2' ><textarea class='edit3' name='ci' id='ci' rows='2' maxsize='4000' >" . $ci . "</textarea></td>";
$pageData .= "</tr><tr>\n";
$pageData .= "<td class='field'>Teir 2:</td><td class='info'><select name='tier2' id='tier2' class='tiers'>";
$pageData .= $tier2;
$pageData .= "</select>\n";
$pageData .= "</td>";
$pageData .= "</tr><tr>\n";
$pageData .= "<td class='field'>Teir 3:</td><td class='info'><select name='tier3' id='tier3' class='tiers'>";
$pageData .= $tier3;
$pageData .= "</select>\n";
$pageData .= "</td><td class='field center' colspan='2'>Incident Troubleshooting</td>";
$pageData .= "</tr><tr>";
$pageData .= "<td class='field'>Password:</td><td class='info'><input class='edit1' type='text' name='pass' id='pass' maxsize='255' value='" . $pass . "' /></td>";
$pageData .= "<td class='info' colspan='2' rowspan='10' ><textarea class='edit3' name='incTrouble' id='incTrouble' maxsize='4000' rows='14'>" . $incTrouble . "</textarea></td>";
$pageData .= "</tr><tr>";
$pageData .= "<td class='field center' colspan='2'>Incident (Multiple)</td>";
$pageData .= "</tr><tr>";
$pageData .= "<td colspan='2' rowspan='3' class='info'><textarea class='edit3' name='incMultiple' id='incMultiple' maxsize='4000' rows='3' >" . $incMultiple . "</textarea></td>";
$pageData .= "</tr><tr>";
$pageData .= "</tr><tr>";
$pageData .= "</tr><tr>";
$pageData .= "<td class='field center' colspan='2'>Access Request</td>";
$pageData .= "</tr><tr>";
$pageData .= "<td class='field'>Approvals</td><td class='info'><input class='edit1' type='text' name='arApproval' id='arApproval' maxsize='255' value='" . $arApproval . "' /></td>";
$pageData .= "</tr><tr>";
$pageData .= "<td class='field'>Prerequisites</td><td class='info'><input class='edit1' type='text' name='arPrereq' id='arPrereq' maxsize='255' value='" . $arPrereq . "' /></td>";
$pageData .= "</tr><tr>";
$pageData .= "<td class='field center' colspan='2' >Work Flow</td>";
$pageData .= "</tr><tr>";
$pageData .= "<td class='info' colspan='2' rowspan='3'><textarea class='edit3' name='arWorkflow' id='arWorkflow' maxsize='400' rows='3'>" . $arWorkflow . "</textarea></td>";
$pageData .= "</tr><tr>";
$pageData .= "<td class='field'>Attachtment</td><td class='info'>Being Worked on.</td>";
                //<input type='file' name='attachment' id='attachment' " . $attachment . " />
$pageData .= "</tr>";
$pageData .= "</table>";
if($_SESSION['auth'] >= QR_ADMIN)
{
    $pageData .= $addArchive;
}
$pageData .= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type='button' onclick='$saveAction' value='Save' />";
$pageData .= "<input type='button' value='Cancel' onclick='$cancelAction' />";
//$pageData .= "<a href='$saveAction' >test</a>";
$pageData .= "</form>";
        

echo $pageData;

$mysqli->close();
?>