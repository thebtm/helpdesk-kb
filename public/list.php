<?php
session_start();
/**
 * @author Brad Mouck
 * @copyright 2011
 */

error_reporting(E_ALL ^ E_NOTICE);

require_once("../private/initialize.php");

$log = new WriteLog(LOG_PATH, "qrglist.log");
 
$pageData = "";    
//$css = "";
//$jsData = "";
$testData;

if($_POST['sent'] == "yes" && $_POST['searchData'] != "")
{
    
    //$searchType = htmlspecialchars($_POST['searchType']);
    $searchData = htmlspecialchars($_POST['searchData']);
    $archived = $_POST['chkArchived'];
    $searchData = htmlspecialchars(str_replace("*", "%", $searchData));
    /*
    $sqlSearch = "SELECT ID, Name, Description, Tier1, AR_Approvals, URL
                    FROM quickreference
                    WHERE $searchType like '$searchData'";
    */
    $sqlCall = "Call qr_List_Search('$searchData'"; //intentionally open, more code to add
    
    $log->write("\$_Post");
    foreach($_POST as $field => $value)
    {
        $log->write("$field: $value");
    }
    
    $log->write("\$archived: $archived -gettype:" . gettype($archived));
    if($archived == "true")
    {
        //$sqlSearch .= " AND archived='0'";
        $sqlCall .= ", true";
    }
    else
    {
        $sqlCall .= ", false";
    }
    
    $sqlCall .= ")";

    $log->write("SQL Call" . $sqlCall);
    
    $result = $mysqli->query($sqlCall);
    if($mysqli->error)
    {
         $log->write("mySQL Error: " . $mysqli->error);
         die("Error with mysqli: {$mysqli->error}");
    }
    $log->write("\$result rows: " . $result->num_rows);
    
    if($result->num_rows > 0)
    {
        $pageData = "<p id='action'><a href='javascript:expandAll();'>Expand All</a> <a href='javascript:hideAllIDs();'>Collapse All</a></p>\n";    
    }
    else
    {
        $pageData ="your search Data returned with no Entries";
    }
    
    while($rows = $result->fetch_array(MYSQLI_ASSOC))
    {
        foreach($rows as $field => $value)
        {
            $log->write("$field: $value");
        }
        $id = $rows['ID'];
        $name = htmlspecialchars($rows['Name']);
        $tier1 =  $rows['wg_name'];
        $tierId = $rows['wg_id'];
        
        if($rows['Description'] == "See below")
        {
            $description = "Please Visit Detailed Page";
        }
        else
        {
            $description = htmlspecialchars($rows['Description'], ENT_QUOTES);
        }
        
        $approval = htmlspecialchars($rows['AR_Approvals']);
        $url = htmlspecialchars($rows['URL']);
        
        if($tier1 == "")
        {
            $tier1 = "no Workgroup Supplied";
        }
        
        $pageData .= ticketData($id, $name, $tier1, $description, $approval, $url, $tierId);
        //$jsData .= "'div" . $id . "', ";
    }
    
    //$jsData = substr($jsData, 0, -2);
    
}
unset($log);

echo $pageData
?>