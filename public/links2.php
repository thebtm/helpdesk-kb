<?php

/**
 * @author Brad Mouck
 * @copyright 2011
 */

$adminAccess = $_SESSION['auth'] >= QR_ADMIN;
$developAcess = $_SESSION['auth'] >= FULL_ADMIN;
if(is_null($log))
{
    require_once("../private/initialize.php");
    $log = new WriteLog(LOG_PATH, "qrgmembers.log");
}
else
{
    //$log->changeFile("qrgmembers.log");
    $log->write("called from another php page");
}
?>

<script>
$(function()
 {
    $.fn.MyMenu = function(divId, divClass)
    {
        var ulColor = "<?php echo $_SESSION['ulColor']; ?>";
        var liColor = "<?php echo $_SESSION['liColor']; ?>";
        $(this).mouseover(function()
        {
            $(divClass).each(function()
            {
                //alert("divID: " + divId + " - this.id: " + this.id);
                if(divId != this.id)
                {
                    $(this).slideUp();
                } 
            });
            $(divId + "> ul").css("background-color", ulColor);
            //B0FFC5
            $(divId).slideDown();
        });
        $(divId).mouseleave(function()
        {
            $(divId).slideUp(); 
        });
        $(this).button();   
        
        $(divId + " > ul > li").mouseover(function()
        {
            $(this).css("background-color", liColor);
        })
        .mouseout(function()
        {
            $(this).css("background-Color", ulColor);
        });
    }
    
    
    $("#btnHome").MyMenu("#menuHome", ".dMenu");
    $("#lnHome").click(function()
    {
        window.location.reload();
    });
    $("#lnBug").click(function()
    {
        alert("Bug clicked");
    });
    $("#lnSettings").click(function()
    {
        getPage("settings.php", "dataPage", null);
    });
    $("#lnLogout").click(function()
    {
        getPage('logout.php','page');
    });
    
    <?php
	if($adminAccess)
	{
	?>
		$("#btnAdmin").MyMenu("#menuAdmin", ".dMenu");
		$("#lnMember").click(function()
		{
			getMembers();
		});
		$("#lnWorkgroup").click(function()
		{
			modifyWorkgroups(0, <?php echo NEW_ENTRY ?>, 'dTeam');
		});
		$("#lnQR").click(function()
		{
			modifyDetails(0, <?php echo NEW_ENTRY; ?>, 'dDetails');
		});
    <?php
	}
	
	if($developAcess)
	{
		?>
		
		$("#btnDev").MyMenu("#menuDev",".dMenu");
		$("#lnTables").click(function()
		{
			alert("Tables");
		});
		$("#lnLogs").click(function()
		{
			getPage('getLogs.php','dataPage');
		});
		$("#lnBugReport").click(function()
		{
			alert("Bug Report"); 
		});
		$("#lnSuggestReport").click(function()
		{
			alert("Suggest Report");
		});
    <?php
	}
	?>
    
    //alert("test");
 });
</script>

<?php

 if(1 == 1)
 {
  ?>
   <div class="dMenu" id="menuHome">
    <ul class="menu">
     <li id="lnHome">Home</li>
     <li id="lnBug">Report a Bug</li>
     <li id="lnFeedback">Suggestion</li>
     <li id="lnSettings">Settings</li>
     <li id="lnLogout">Logout</li>
    </ul>
   </div>
  <?php
 }
 if($adminAccess)
 {
    ?>
   <div class="dMenu" id="menuAdmin">
    <ul class="menu">
     <li id="lnMember">Members</li>
     <li id="lnWorkgroup">Add Workgroup</li>
     <li id="lnQR">Add Quickreference</li>
     <li>Attachment Management</li>
    </ul>
   </div>
    <?php
 }
 
 if($developAcess)
 {
    ?>
   <div class="dMenu" id="menuDev">
    <ul class="menu">
     <li id="lnTable">Tables</li>
     <li id="lnLogs">Error Logs</li>
     <li id="lnBugReport">Bug Report</li>
     <li id="lnSuggestReport">Suggestion Report</li>
    </ul>
   </div>
    <?php
 }
 
?>
<div id="linksx">
 <table class="linksx">
  <tr>
  <td>
  <?php
   if(true)
   { 
   ?>
    <button id="btnHome" >Home</button>
   <?php 
   }
   
   if($adminAccess)
   {
   ?>
    <button id="btnAdmin">Administrator</button>
   <?php
   }
   
   if($developAcess)
   {
   ?>
     <button id="btnDev">Developer</button>
   <?php
   }
  ?>
  </td>
  </tr>
 </table>
</div>

