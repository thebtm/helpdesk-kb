//old method, this function is to get details
function showDetails(id, strDiv, type)
{
    var xmlhttp;
    var objDiv = document.getElementById(strDiv);
    
    if (id.length == 0)
    {
        objDiv.innerHTML = "";
        return;
    }
    
    if (window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else
    {
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP"); 
    }
    
    xmlhttp.onreadystatechange = function()
    {
        if(xmlhttp.readyState == 4 && xmlhttp.status == 200)
        {
            objDiv.innerHTML = xmlhttp.responseText;
            strDiv = "#" + objDiv.id;
            //alert(strDiv);
            $(strDiv).dialog({
                minWidth: 750,
                modal: true
                
            });
            //centerDetails();
        }
    }
    
    xmlhttp.open("GET", "details.php?id=" + id + "&type=" + type, true);
    xmlhttp.send();
}

//this function is to get the details before a save (jquery ajax)
function showDetailsAjax(id, strDiv, type)
{
    strDiv = "#" + strDiv;
    //strData = "{id:" + id + ", type: " + type + "}";
    //$(strDiv).html(ajax_load)
    $.get("./details.php", 
            {id: id, type: type},
            function(data)
            {
                //alert(data);
                $(strDiv).html(data);
                $(strDiv).dialog(
                {
                    minWidth: 750,
                    minHeight: 150,
                    modal: true
                });
            },
            "text");
    
}

//this function is to get the details after a Save (jquery ajax)
function getDetails(id, type, objDiv)
{
    var output;
    ouput =
        $.get("./details.php",
        {id: id, type: type},
        function(data)
        {
            objDiv.innerHTML += data;
            //alert(data);
            //alert(output);
        }
        );
    
    return output;
}

//this function is to get the addmodify php for updating quickreference guide. (jquery ajax)
function modifyDetails(id, type, strDiv)
{
    var xmlhttp;
    var objDiv = document.getElementById(strDiv);
    
    if (id.length == 0)
    {
        objDiv.innerHTML = "";
        return;
    }
    
    if (window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else
    {
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP"); 
    }
    
    xmlhttp.onreadystatechange = function()
    {
        if(xmlhttp.readyState == 4 && xmlhttp.status == 200)
        {
            objDiv.innerHTML = xmlhttp.responseText;
           $("#"+strDiv).dialog({
                minWidth: 750,
                minHeight: 750,
                modal: true
            });
            //$('select').selectmenu();
            //centerDetails();
        }
    }
    
    xmlhttp.open("GET", "addmodify.php?id=" + id + "&status=" + type, true);
    xmlhttp.send();
}

//to modify workgroups (ajax)
function modifyWorkgroups(id, type, strDiv)
{
    
    var objDiv = document.getElementById(strDiv);
    
    if (id.length == 0)
    {
        objDiv.innerHTML = "";
        return;
    }
    $.get("addmodifyworkgroups.php",
        {id: id, status: type},
        function(data)
        {
            $("#"+strDiv).html(data);
            $("#"+strDiv).dialog({
                minWidth: 750,
                //minHeight: 100,
                modal: true
            });
            //$('select').selectmenu();
            //centerDetails();
        },
        "html")
        .error(function()
        {
            alert("An Error Has Occured");
        });
    

}

function saveDetails(id, intStatus, strWhat, strDiv)
{
    var xmlhttp;
    var objDiv = document.getElementById(strDiv);
    
    if (id.length == 0)
    {
        objDiv.innerHTML = "";
        return;
    }
    
    if (window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else
    {
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP"); 
    }
    
    xmlhttp.onreadystatechange = function()
    {
        if(xmlhttp.readyState == 4 && xmlhttp.status == 200)
        {
            objDiv.innerHTML = xmlhttp.responseText + "<br />";
            strOutput = xmlhttp.responseText;
            
            if(intStatus == 3)
            {
                getDetails(id, strWhat, objDiv);
            }
            else if(intStatus == 0)
            {
                //from website
                var priceAsFloat = function (price) 
                {  
                    return parseFloat(price.replace(/\./g, '').replace(/,/g,'.').replace(/[^\d\.]/g,''), 10);
                }

                id = priceAsFloat(strOutput);
                getDetails(id, strWhat, objDiv);
            }
            $("#"+strDiv).dialog({
                minWidth: 750,
                //minHeight: 750,
                modal: true
            });
            
            //centerDetails();
        }
    }
    
    var myForm = document.forms[1];
    var sendData = getRequestBody(myForm);
    //alert(sendData);
    
    xmlhttp.open("POST", "save.php?id=" + id + "&status=" + intStatus + "&what=" + strWhat, true);
    xmlhttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xmlhttp.send(sendData);
}

//got this from the internet, javascript.about.com, 
/*

gets all elements in the form and information in the form then sets it up for it to be sent by AJAX
to the server.

*/
function getRequestBody(theForm) 
{
    var p = [];
    for (var i = theForm.elements.length-1; i >= 0; i--) 
    {
        var fld = theForm.elements[i];
        
        switch (fld.type) 
        {
            case "button": case "submit": case "reset": break;
            case "checkbox": case "radio": if (!fld.checked) break;
            default:
            if ("select" == fld.tagName.toLowerCase())
                p.push(ncode(fld.name,fld.options[fld.selectedIndex].value));
            else 
                p.push(ncode(fld.name,fld.value));
        }
    }
    return p.join('&');
}

//got this from the internet, javascript.about.com,
function ncode(n,v) 
{
    v = v.replace("'", "\'")
    return encodeURIComponent(n) + '=' + encodeURIComponent(v);
}

function centerDetails()
{
    var intWidth = screen.width;
    //alert(intWidth);
    document.getElementById("dDetails").style.left = ((intWidth - 700) /2);
    document.getElementById("dTeam").style.left = ((intWidth - 500) /2);
    document.getElementById("dTeam").style.top = ((screen.height ) / 10);
}

function closeDiv(id)
{
    document.getElementById(id).style.display = "none";
    $("#"+id).dialog("close");
    if(id == "dDetails")
    {
        document.getElementById(id).innerHTML = "&nbsp;";
    }
}

function showDiv(id)
{
    document.getElementById(id).style.display = "block";
}



function getList()
{
    strSearchType = $("#searchType").val();
    strSearchData = $("#searchData").val();
    blArchived = $("#chkArchived").is(":checked");
    
    //alert("type: " + strSearchType + " - data: " + strSearchData + " - archived: " + blArchived);
    
    $.post("list.php",
            {searchType: strSearchType, searchData: strSearchData, chkArchived: blArchived, sent: "yes"},
            function(data)
            {
                //alert(data);
                $("#list").html(data);
            },
            "html")
     .error(function()
     {
        alert("An Error Has Occured, @JQuery.AJAX:list.php");
     });
}

function sortGroup()
{
    $.get("listbygroup.php",
            null,
            function(data)
            {
                $("#list").html(data);
            },
            "html")
    .error(function()
    {
        alert("An Error Has Occured, @JQuery.AJAX:listbygroup.php");
    });
}

function emailCheck()
{
    var d = document;
    var email1 = d.getElementById("email1").value;
    
}

function getMembers()
{
    $.get("members.php",
            null,
            function(data)
            {
                $("#dataPage").html(data);
            },
            "html");
}

function getPage(page, divID, data)
{
    data = typeof(data) != 'undefined' ? data : null;
    $.get(page,data,
        function(returnData)
        {
            $("#"+divID).html(returnData);
            //alert(returnData);
        },
        "html");
}

function postPage(page, divID, form)
{
    form = typeof(form) != 'undefined' ? form : null;
    var data = $(form).serialize();
    $.post(page,data,
        function(returnData)
        {
            $("#"+divID).html(returnData);
            //alert(returnData);
        },
        "html");
}

function postMembers(form)
{
    var postData = form.serialize();
    //alert(postData);
    if(postData.indexOf("yes") >= 0)
    {
        $.post("saveMembers.php",
            postData,
            function(data)
            {
                //getMembers();
                $("#dataPage").html(data);
            },
            "html");
    }
    else
    {
        alert("No Access was changed");
    }       
}