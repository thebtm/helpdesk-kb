<?php
session_start();
/**
 * @author Brad Mouck
 * @copyright 2011
 */

error_reporting(E_ALL ^ E_NOTICE);

require_once("../private/initialize.php");

$log = new WriteLog(LOG_PATH, "listbygroup.log");

$log->write("List By Group");

if($_SESSION['auth'] >= QR_ADMIN)
{
    $sqlCall = "Call select_qr_wg_by_Group(true)";
}
else
{
    $sqlCall = "Call select_qr_wg_by_Group(false)";
}
$log->write($sqlCall);
$result = $mysqli->query($sqlCall);
if($mysqli->error)
{
    $log->write($mysqli->error);
    unset($log);
    die("Error with My SQL: " . $mysqli->error);
}

//$pageData = "<table>";
while($row = $result->fetch_array(MYSQLI_ASSOC))
{
    //$pageData .= "<tr>";
    foreach($row as $field => $value)
    {
        if($value == null)
        {
            $value = "Null ";   
        }
            
        //$log->write("$field: $value");
        //$pageData .= "<td>$field: $value</td>";
    }
    if($group != $row['wg_name'])
    {
        $tierID = $row['wg_id'];
        $pageData .= "<br />";
        $pageData .= "<a class='title' href='javascript:showDetailsAjax($tierID, \"dTeam\", \"" . WG . "\");'>" . $row['wg_name'] . "</a>";
        if($row['OncallPhone'] != null)
        {
            $pageData .= " - " . $row['OncallPhone'];
        }
        else
        {
            $pageData .= " - No Number Supplied";
        }
        
        if($_SESSION['auth'] >= QR_ADMIN && $row['wg_archived'] == 1)
        {
            $pageData .= " - Archived";
        }
        
        $pageData .= "<hr class='undertitle' />";
    }
    $group = $row['wg_name'];
    
    if($row['Tier1'] != null)
    {
        $pageData .= ticketData($row['ID'], $row['Name'], $row['wg_name'], $row['Description'], $row['AR_Approval'], $row['URL'], $row['Teir1'] );
    }
    
    //$pageData .= "</tr>";
}
//$pageData .= "</table>";

echo $pageData;

?>