<?php
session_start();
/**
 * @author Brad Mouck
 * @copyright 2011
 */

//error_reporting(E_ALL);
error_reporting(E_ALL ^ E_NOTICE);
require_once("../private/initialize.php");

$log = new WriteLog(LOG_PATH, "qrgindex.log");

if($_SESSION['auth'] < QR_ACCESS)
{
    $log->write("Authentication load");
    require("./authMenu.php");
    exit();
}
elseif($_SESSION['auth'] >= QR_ACCESS)
{
    $log->write($_SESSION['username']);
    if($_SESSION['username'] != ROOT)
    {
        $_SESSION['auth'] = confirmAccess($_SESSION['username'], $log, $mysqli);
    }
    else
    {
        $log->write("skipped check - root");
    }
    $log->write("userCSS: " . $_SESSION['userCSS']);
    require("./getcolorsettings.php");
}

// html header
$browser = get_browser($_SERVER['HTTP_USER_AGENT'], true);
$log->write("Browser check:" . substr($browser['parent'], 0, 2));
if(strpos($_SERVER['HTTP_USER_AGENT'], "MSIE 9") > 0)
{
    echo "<!DOCTYPE html public \"-//W3C//DTD XHMTL 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">";
    echo "<html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"en\" lang=\"en\">";
}
else
{
    echo"<html>";
}


//page header
?>
<head>
<link rel="stylesheet" href="colorpicker/farbtastic.css" type="text/css" />
<?php
if(isset($_SESSION['JQuery']))
{
?>
 <link rel="stylesheet" type="text/css" href="./css/<?php echo $_SESSION['JQuery']; ?>/jquery-ui-1.8.16.custom.css" />
<?php
}
else
{
?>
 <link rel="stylesheet" type="text/css" href="css/ui-lightness/jquery-ui-1.8.16.custom.css" /> <!-- JQuery is GPL -->
 <?php
}
  //<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" /> 
  ?>
 <link rel="stylesheet" type="text/css" href="css/qrgpages.css" />

 <link rel="stylesheet" type="text/css" href="css/qrlinks.css" />

 <?php

 if($_SESSION['auth'] == QR_ADMIN)
 {
    ?><link rel="stylesheet" type="text/css" href="css/qrlinksadmin.css" /> <?php
 }
 elseif($_SESSION['auth'] ==  QR_ACCESS)
 {
    ?><link rel="stylesheet" type="text/css" href="css/qrlinksaccess.css" /> <?php
 }
 ?>
 
 
 <?php
 if(substr($browser['parent'], 0, 2) == "IE")
{?>
  <link rel="stylesheet" type="text/css" href="css/qrg_ie.css" />
  <link rel="stylesheet" type="text/css" href="css/qrlinks_ie.css" />
<?php    
}
if(isset($_SESSION['customCSS']))
{
    ?>
    <link rel="stylesheet" type="text/css" href="<?php echo $_SESSION['customCSS']; ?>" />
    <?php
}
 ?>

 <script type="text/javascript" src="js/jquery-1.6.2.min.js" >//JQuery load - JQuery is GPL</script>
 <script type="text/javascript" src="js/jquery-ui-1.8.16.custom.min.js" >//JQuery UI load - JQuery UI is GPL</script>
 <script type="text/javascript" src="js/ajax.js" ></script>
 <script type="text/javascript" src="colorpicker/farbtastic.js"></script>

 
 <script type="text/javascript"> 
 sortGroup();
 
 function DivShow(id)
 {
     //var objDiv = document.getElementById(id);
     hideAllIDs();
     $("#"+id).show(200);
     //objDiv.style.display = "block";
   
 }
 
 function hideAllIDs()
 {
     $("#list > div").hide(100);
 }
 
 function expandAll()
 {
     $("#list > div").show(100);
 }

 </script>
</head>
<body>
 <div id="page" class="page">
 <div id="dTeam" ></div>
 <div id="dDetails"></div>
<?php
require("./pageheader.htm");
//links
if($_SESSION['LinkSystem'] == LIST_MENU)
{
    require("./links2.php");
}
else
{
    require("./links.php");
}

?>
<div id="dataPage">
<?php
//search
require("./searchMenu.htm");
?>
    <div id="list">Loading ...</div>
</div>
<?php

// footer
$mysqli->close();
?>
 </div>
<footer>
    Created by Brad Mouck.
</footer>
</body>
</html>