<?php

/**
 * @author Brad Mouck
 * @copyright 2011
 */
 
session_start();
error_reporting(E_ALL ^ E_NOTICE);

require_once("../private/initialize.php");

$log = new WriteLog(LOG_PATH, "qrg_details.log");

//error_reporting(E_ALL);
$id = $_GET['id'];
$type = $_GET['type'];

foreach($_GET as $f => $v)
{
    $log->write("get $f: $v");
}

if($id != "" && $type != "")
{
    $pageData = "";
    
    if($type == "qrg")
    {
        $table = "quickreference";
    }
    elseif($type == "wg")
    {
        $table = "workgroups";
    }
    else
    {
        die("Error has Occurred at Type. @details.php:type:$type");
    }
    
    $sqlDetails = "Select * from $table where id='$id'";
    $log->write("sql $sqlDetails");
    
    $result = $mysqli->query($sqlDetails);
    $row = $result->fetch_array(MYSQLI_ASSOC);
    
    foreach($row as $field => $value)
    {
        if($value == null)
        {
            $row["$field"] = "NA";
        }
        $log->write("$feild: $value");
    }
    
    if($type == QRG)
    {
        $sqlTeir = "select ID, Name, Archived from workgroups";
        $wgResult = $mysqli->query($sqlTeir);
        
        $id = $row['ID'];
        $name = htmlspecialchars($row['Name']);
        $description = nl2br(htmlspecialchars($row['Description']));
        $ticketDesc = htmlspecialchars($row['TicketDescription']);
        $keywords = htmlspecialchars($row['Keywords']);
        $selfhelp = htmlspecialchars($row['SelfHelp']);
        $tier1['id'] = $row['Tier1'];
        $tier2['id'] = $row['Tier2'];
        $tier3['id'] = $row['Tier3'];
        
        while($wgRow = $wgResult->fetch_array(MYSQLI_ASSOC))
        {
            foreach ($wgRow as $field => $value)
            {
                If($row['Tier1'] == $wgRow['ID'])
                {
                    $tier1['name'] = $wgRow['Name'];
                }
                
                If($row['Tier2'] == $wgRow['ID'])
                {
                    $tier2['name'] = $wgRow['Name'];
                }
                
                If($row['Tier3'] == $wgRow['ID'])
                {
                    $tier3['name'] = $wgRow['Name'];
                }
            }
        }
        
        $pass = htmlspecialchars($row['PasswordUsed']); 
        $incMultiple = nl2br(htmlspecialchars($row['INMultiple']));
        $url = htmlspecialchars($row['URL']);
        $ci = nl2br(htmlspecialchars($row['CI']));
        $incTrouble = nl2br(htmlspecialchars($row['IncidentTroubleshooting']));
        $arApproval = htmlspecialchars($row['AR_Approvals']);
        $arPrereq = htmlspecialchars($row['AR_Prerequisites']);
        $arWorkflow = nl2br(htmlspecialchars($row['AR_Workflow']));
        $businessOwner = htmlspecialchars($row['BusinessOwner']);
        $businessHours = htmlspecialchars($row['BusinessHours']);
        $supportHours = htmlspecialchars($row['BusinessHours']);
        $stakeholders = htmlspecialchars($row['Stakeholders']);
        $attachment = $row['Attachment_ID'];
        
        $pageData .= "<table id='details'>";
        $pageData .= "<tr>";
        $pageData .= "<td class='field d1'>Name:</td><td class='info d2'>" . $name . "</td><td class='field d3'>Business Owner</td><td class='info d4'>" . $businessOwner . "</td>";
        $pageData .= "</tr><tr>";
        $pageData .= "<td class='field'>Description:</td><td class='info' rowspan='2'>" . $description . "</td><td class='field'>Stakeholders</td><td class='info'>" . $stakeholders . "</td>";
        $pageData .= "</tr><tr>";
        $pageData .= "<td></td><td class='field'>Business Hours</td><td class='info'>" . $businessHours . "</td>";
        $pageData .= "</tr><tr>";
        $pageData .= "<td class='field'>Keywords: </td><td class='info'>$keywords</td><td class='field'>IT Support Hours</td><td class='info'>" . $supportHours . "</td>";
        $pageData .= "</tr><tr>";
        $pageData .= "<td colspan='2' class='field center'>Incident (Single)</td><td class='field'>URL</td><td class='info' >"; 
        if(strtolower(substr($url, 0, 4)) == "http")
        {
            $pageData .= "<a href='$url' target='_blank'>$url</a>";
        }
        else
        {
            $pageData .= $url;
        }
        $pageData .= "</td>";
        $pageData .= "</tr><tr>";
        $pageData .= "<td class='field'>Self Help:</td><td class='info'>" . $selfhelp . "</td><td class='field center' colspan='2'>CI</td>";
        $pageData .= "</tr><tr>";
        $pageData .= "<td class='field'>Teir 1:</td><td class='info'>";
        
        if($tier1['id'] != "NA")
        {
            $pageData .= "<a href='javascript:showDetailsAjax(" . $tier1['id'] . ", \"dTeam\", \"" . WG . "\");showDiv(\"fade2\");' >" . $tier1['name'] . "</a>";
        }
        else
        {
            $pageData .= $tier1['id'];
        }
        $pageData .= "</td><td class='info' colspan='2' rowspan='2' >" . $ci . "</td>";
        $pageData .= "</tr><tr>";
        $pageData .= "<td class='field'>Teir 2:</td><td class='info'>";
        if($tier2['id'] != "NA")
        {
            $pageData .= "<a href='javascript:showDetailsAjax(" . $tier2['id'] . ", \"dTeam\", \"" . WG . "\");showDiv(\"fade2\")' >" . $tier2['name'] . "</a>";
        }
        else
        {
            $pageData .= $tier2['id'];
        }
        $pageData .= "</td>";
        $pageData .= "</tr><tr>";
        $pageData .= "<td class='field'>Teir 3:</td><td class='info'>";
        if($tier3['id'] != "NA")
        {
            $pageData .= "<a href='javascript:showDetailsAjax(" . $tier3['id'] . ", \"dTeam\", \"" . WG . "\");showDiv(\"fade2\")' >" . $tier3['name'] . "</a>";
        }
        else
        {
            $pageData .= $tier3['id'];
        }
        $pageData .= "</td><td class='field center' colspan='2'>Incident Troubleshooting</td>";
        $pageData .= "</tr><tr>";
        $pageData .= "<td class='field'>Password:</td><td class='info'>" . $pass . "</td><td class='info' colspan='2' rowspan='10' >" . $incTrouble . "</td>";
        $pageData .= "</tr><tr>";
        $pageData .= "<td class='field center' colspan='2'>Incident (Multiple)</td>";
        $pageData .= "</tr><tr>";
        $pageData .= "<td colspan='2' rowspan='3' class='info'>" . $incMultiple . "</td>";
        $pageData .= "</tr><tr>";
        $pageData .= "</tr><tr>";
        $pageData .= "</tr><tr>";
        $pageData .= "<td class='field center' colspan='2'>Access Request</td>";
        $pageData .= "</tr><tr>";
        $pageData .= "<td class='field'>Approvals</td><td class='info'>" . $arApproval . "</td>";
        $pageData .= "</tr><tr>";
        $pageData .= "<td class='field'>Prerequisites</td><td class='info'>" . $arPrereq . "</td>";
        $pageData .= "</tr><tr>";
        $pageData .= "<td class='field center' colspan='2' >Work Flow</td>";
        $pageData .= "</tr><tr>";
        $pageData .= "<td class='info' colspan='2' rowspan='2'>" . $arWorkflow . "</td>";
        $pageData .= "</tr><tr>";
        $pageData .= "<td class='field'>Attachtment</td><td class='info'>" . $attachment . "</td>";
        $pageData .= "</tr>";
        
        if($_SESSION['auth'] > QR_ADMIN)
        {
            $pageData .= "<tr><td colspan='4'>Last Modified By: {$row['LastModBy']} On {$row['LastModDate']}</td></tr>";
        }
        
        $pageData .= "</table>";
        
        if($_SESSION['auth'] > QR_ACCESS)
        {
            $pageData .= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type='button' value='Modify' onclick='javascript:modifyDetails($id, " . MODIFY_ENTRY . ", \"dDetails\");' />";
            
        }
        $pageData .= "<input type='button' onclick=\"javascript:closeDiv('dDetails');\" value='Close' />";
        //$pageData .= "<a href='javascript:modifyDetails($id, " . MODIFY_ENTRY . ", \"dDetails\");'>test</a>";
        
        //javascript:showDetailsAjax($id, \"dDetails\", \"" . QRG . "\");
             
        echo $pageData;
    }
    elseif($type == WG)
    {
        $sqlTeir = "select * from workgroups where id='$id'";
        $log->write("SQL wg: " . $sqlTeir);
        $wgResult = $mysqli->query($sqlTeir);
        $wgRow = $wgResult->fetch_array(MYSQLI_ASSOC);
        
        
        foreach($wgRow as $field => $value)
        {
            //$pageData .= "$field: $value \n<br />";
            $wgRow[$field] = htmlspecialchars($value);
        }
        
        $id = $wgRow['ID'];
        $name = htmlspecialchars($wgRow['Name']);
        $searchCode = htmlspecialchars($wgRow['SearchCode']);
        $email[1] = htmlspecialchars($wgRow['EmailAddress1']);
        $email[2] = htmlspecialchars($wgRow['EmailAddress2']);
        $email[3] = htmlspecialchars($wgRow['EmailAddress3']);
        $phone = htmlspecialchars($wgRow['OncallPhone']);
        
        $pageData = "";
        $pageData .= "<table>";
        $pageData .= "<tr><td class='field'>Name:</td><td class='info'>$name</td><td class='field'>Search Code:</td><td class='info'>$searchCode</td></tr>";
        foreach($email as $field => $value)
        {
            if($value != "")
            {
                $pageData .= "<tr><td class='field'>Email $field:</td><td colspan='3' class='info'>$value</td></tr>";
            }
            elseif($field == 1)
            {
                $pageData .= "<tr><td class='field'>Email $field:</td><td colspan='3' class='info'>No Email Documented</td></tr>";
            }
        }
        $pageData .= "<tr><td class='field'>On Call Phone</td><td class='info' colspan='3'>$phone</td></tr>"; 
        if($_SESSION['auth'] > QR_ADMIN)
        {
            $pageData .= "<tr><td colspan='4' class='field'>Last Modified By: {$row['LastModBy']} On {$row['LastModDate']}</td></tr>";
        }
        $pageData .= "</table>";
        if($_SESSION['auth'] > QR_ACCESS)
        {
            $pageData .= "<input type='button' onclick=\"javascript:modifyWorkgroups('$id', ' " . MODIFY_ENTRY . "', 'dTeam');\" value='Modify' />";
        }
        $pageData .= "<input type='button' onclick=\"javascript:closeDiv('dTeam');\" value='Close' />";
        echo $pageData;
    }
}
else
{
    echo "An Error Has Occured.";
}

$mysqli->close();
?>