<?php
session_start();
error_reporting(E_ALL ^ E_NOTICE);
/**
 * @author Brad Mouck
 * @copyright 2011
 */
require_once ("../private/initialize.php");

$log = new WriteLog(LOG_PATH, "qrSaveMembers.log");

if($_SESSION['auth'] >= QR_ADMIN)
{
    
    foreach($_POST as $field => $value)
    {
        if(substr($field, 0, 2) == "id")
        {
            $id = $value;
        }
        if(substr($field, 0, 6) == "update")
        {
            $data[$id]['update'] = $value;
        }
        elseif(substr($field,0,strlen("siteaccess")))
        {
            $data[$id]['siteaccess'] = $value;
        }
    }
    
    foreach($data as $field => $value)
    {
        $log->write("\$data: $field: $value \n");
        if(is_array($value))
        {
            foreach($value as $f => $v)
            {
                $log->write("&nbsp; &nbsp $value: $f: $v \n");
                if($v == "yes")
                {
                    $sqlCall = "Call update_access($field, {$data[$field]['siteaccess']})";
                    $mysqli->query($sqlCall);
                    if($mysqli->error)
                    {
                        $log->write("$sqlCall: \n    " . $mysqli->error);
                    }
                }
            }
        }
    }

    include("members.php");

}
else
{
    header("location:./");
}
?>