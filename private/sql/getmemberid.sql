DELIMITER $$

DROP PROCEDURE IF EXISTS `getMemberID` $$

CREATE PROCEDURE `getMemberID` (
    s_Username varchar(45))
BEGIN
    select id from members where Username=s_Username;
END $$

DELIMITER ;
