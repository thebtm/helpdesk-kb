DELIMITER $$

DROP PROCEDURE IF EXISTS `getColorSettings` $$

CREATE PROCEDURE `getColorSettings` (
m_id integer(4),
m_count integer(4))
BEGIN

    IF m_count = 0 THEN
select *
from settings
where MemberID = m_id
order by id desc;
ELSE
set @limit = m_count;
set @m_id = m_id;

prepare sqlStatement from
    "select *
    from settings
    where MemberID = ?
    order by id desc
    limit 0, ?";

execute sqlStatement using @m_id, @limit;

deallocate prepare sqlStatement;
END IF;

END $$

DELIMITER ;