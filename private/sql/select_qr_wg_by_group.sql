DELIMITER $$

DROP PROCEDURE IF EXISTS `select_qr_wg_by_group` $$

CREATE PROCEDURE `select_qr_wg_by_group`(s_archived boolean)
BEGIN
    IF s_archived THEN
        SELECT  `quickreference`.`ID` ,  `quickreference`.`Name` ,  `quickreference`.`Description` ,  `quickreference`.`Tier1` ,  `quickreference`.`URL` , `quickreference`.`Tier1`, `quickreference`.`AR_Approvals`,  `workgroups`.`ID` as wg_id ,  `workgroups`.`Name` as wg_name ,  `workgroups`.`archived` as wg_archived, `workgroups`.`OncallPhone` as OncallPhone
        FROM  `workgroups`
                  LEFT JOIN  `quickreference` ON  `workgroups`.`ID` =  `quickreference`.`Tier1`
        ORDER BY  `workgroups`.`Name` ASC, `quickreference`.`Name` ASC;
    ELSE
        SELECT  `quickreference`.`ID` ,  `quickreference`.`Name` ,  `quickreference`.`Description` ,  `quickreference`.`Tier1` ,  `quickreference`.`URL` , `quickreference`.`Tier1`, `quickreference`.`AR_Approvals`,  `workgroups`.`ID` as wg_id ,  `workgroups`.`Name` as wg_name ,  `workgroups`.`archived` as wg_archived, `workgroups`.`OncallPhone` as OncallPhone
        FROM  `workgroups`
                  LEFT JOIN  `quickreference` ON  `workgroups`.`ID` =  `quickreference`.`Tier1`
        WHERE `workgroups`.`archived` = 0
        ORDER BY  `workgroups`.`Name` ASC, `quickreference`.`Name` ASC;
    END IF;
END $$

DELIMITER ;