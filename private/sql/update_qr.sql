DELIMITER $$

DROP PROCEDURE if exists `update_qr` $$

CREATE PROCEDURE `update_qr` (
    up_ID int(4),
    up_Name varchar(255),
    up_Description longtext,
    up_TicketDescription varchar(100),
    up_keywords varchar(50),
    up_SelfHelp varchar(255),
    up_Tier1 int(4),
    up_Tier2 int(4),
    up_Tier3 int(4),
    up_PasswordUsed varchar(255),
    up_INMultiple longtext,
    up_URL varchar(255),
    up_CI varchar(255),
    up_IncidentTroubleshooting longtext,
    up_AR_Approvals varchar(255),
    up_AR_Prerequisites varchar(255),
    up_AR_Workflow longtext,
    up_BusinessOwner varchar(255),
    up_Stakeholders varchar(255),
    up_BusinessHours varchar(255),
    up_SupportHours varchar(255),
    up_Attachment_ID int(4),
    up_archived int(1),
    up_ModBy varchar(45))
BEGIN
    UPDATE quickreference
    SET Name=up_Name,
        Description=up_Description,
        TicketDescription=up_TicketDescription,
        Keywords=up_keywords,
        SelfHelp=up_SelfHelp,
        Tier1=up_Tier1,
        Tier2=up_Tier2,
        Tier3=up_Tier3,
        PasswordUsed=up_PasswordUsed,
        INMultiple=up_INMultiple,
        URL=up_URL,
        CI=up_CI,
        IncidentTroubleshooting=up_IncidentTroubleshooting,
        AR_Approvals=up_AR_Approvals,
        AR_Workflow=up_AR_Workflow,
        BusinessOwner=up_BusinessOwner,
        Stakeholders=up_Stakeholders,
        BusinessHours=up_BusinessHours,
        SupportHours=up_SupportHours,
        Attachment_ID=up_Attachment_ID,
        archived=up_archived,
        LastModBy=up_ModBy,
        LastModDate=CURRENT_TIMESTAMP
    where ID=up_ID;

END $$

DELIMITER ;