DELIMITER $$

DROP PROCEDURE IF EXISTS `get_access_levels` $$

    CREATE PROCEDURE `get_access_levels` ()
BEGIN
    SELECT * FROM siteaccess Order by id asc;
END $$

DELIMITER ;