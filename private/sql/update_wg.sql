DELIMITER $$

DROP PROCEDURE IF EXISTS `update_wg` $$

CREATE PROCEDURE `update_wg` (
    up_id int(4),
    up_name varchar(75),
    up_searchcode varchar(50),
    up_email1 varchar(100),
    up_email2 varchar(100),
    up_email3 varchar(100),
    up_phone varchar(14),
    up_archived int(1),
    up_ModBy varchar(45))
BEGIN
    UPDATE workgroups
    SET Name=up_name,
        SearchCode=up_searchcode,
        EmailAddress1=up_email1,
        EmailAddress2=up_email2,
        EmailAddress3=up_email3,
        OncallPhone=up_phone,
        archived=up_archived,
        LastModBy=up_ModBy,
        LastModDate=CURRENT_TIMESTAMP
    WHERE id=up_id;
    select id from workgroups where id=up_id;
END $$

DELIMITER ;