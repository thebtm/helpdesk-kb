DELIMITER $$

DROP PROCEDURE IF EXISTS `insert_qr` $$

create procedure insert_qr(
    in_Name varchar(255),
    in_Description longtext,
    in_TicketDescription varchar(100),
    in_keywords varchar(50),
    in_SelfHelp varchar(255),
    in_Tier1 int(4),
    in_Tier2 int(4),
    in_Tier3 int(4),
    in_PasswordUsed varchar(255),
    in_INMultiple longtext,
    in_URL varchar(255),
    in_CI varchar(255),
    in_IncidentTroubleshooting longtext,
    in_AR_Approvals varchar(255),
    in_AR_Prerequisites varchar(255),
    in_AR_Workflow longtext,
    in_BusinessOwner varchar(255),
    in_Stakeholders varchar(255),
    in_BusinessHours varchar(255),
    in_SupportHours varchar(255),
    in_Attachment_ID int(4),
    in_moduser varchar(45)
    )
    MODIFIES SQL DATA
begin
    insert into quickreference
        (ID,
         Name,
         Description,
         TicketDescription,
         keywords,
         SelfHelp,
         Tier1,
         Tier2,
         Tier3,
         PasswordUsed,
         INMultiple,
         URL,
         CI,
         IncidentTroubleshooting,
         AR_Approvals,
         AR_Prerequisites,
         AR_Workflow,
         BusinessOwner,
         Stakeholders,
         BusinessHours,
         SupportHours,
         Attachment_ID,
         archived,
         LastModBy,
         LastModDate)
    values
        (null,
         in_Name,
         in_Description,
         in_TicketDescription,
         in_keywords,
         in_SelfHelp,
         in_Tier1,
         in_Tier2,
         in_Tier3,
         in_PasswordUsed,
         in_INMultiple,
         in_URL,
         in_CI,
         in_IncidentTroubleshooting,
         in_AR_Approvals,
         in_AR_Prerequisites,
         in_AR_Workflow,
         in_BusinessOwner,
         in_Stakeholders,
         in_BusinessHours,
         in_SupportHours,
         in_Attachment_ID,
         0,
         in_moduser,
         current_timestamp);
    select id from quickreference order by id desc limit 0,1;
end $$

delimiter ;