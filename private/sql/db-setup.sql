-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jun 14, 2019 at 03:43 PM
-- Server version: 10.3.13-MariaDB-2
-- PHP Version: 7.2.19-0ubuntu0.19.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hdkb`
--

-- --------------------------------------------------------

--
-- Table structure for table `attactments`
--

CREATE TABLE `attactments` (
  `id` int(11) NOT NULL,
  `name` varchar(30) NOT NULL,
  `type` varchar(30) NOT NULL,
  `size` int(11) NOT NULL,
  `content` longblob NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `bugreport`
--

CREATE TABLE `bugreport` (
  `ID` int(10) UNSIGNED NOT NULL,
  `ActionTaken` varchar(100) NOT NULL,
  `Details` longtext NOT NULL,
  `WorkStatus` varchar(10) NOT NULL DEFAULT 'New',
  `Solution` longtext DEFAULT NULL,
  `EnteredBy` varchar(50) NOT NULL,
  `EnteredDate` timestamp NOT NULL DEFAULT current_timestamp(),
  `ModBy` varchar(50) DEFAULT NULL,
  `ModDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bugreport`
--

INSERT INTO `bugreport` (`ID`, `ActionTaken`, `Details`, `WorkStatus`, `Solution`, `EnteredBy`, `EnteredDate`, `ModBy`, `ModDate`) VALUES
(1, 'saving', 'a error with SQL statement showed up', 'checked', 'currently looking into the option', 'root', '2011-08-23 15:45:13', 'root', '2011-08-23 21:32:37'),
(2, 'saving', 'a error with SQL statement showed up', 'New', NULL, 'root', '2011-08-23 15:47:11', NULL, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `members`
--

CREATE TABLE `members` (
  `ID` int(11) NOT NULL,
  `Username` varchar(45) NOT NULL,
  `AccessLevel` int(1) NOT NULL,
  `Passwrd` varchar(255) DEFAULT NULL,
  `LastSignIn` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='LogUserSignin';

--
-- Dumping data for table `members`
--

INSERT INTO `members` (`ID`, `Username`, `AccessLevel`, `Passwrd`, `LastSignIn`) VALUES
(0, 'root', 9999, '5d65122cf38e21dac5e870fd2427a8d0', '2013-04-10 04:27:43'),
(2, 'brad', 9, NULL, '2013-05-04 05:42:21'),
(3, 'test', 1, NULL, '2013-05-04 05:34:04');


-- --------------------------------------------------------

--
-- Table structure for table `quickreference`
--

CREATE TABLE `quickreference` (
  `ID` int(4) NOT NULL,
  `Name` varchar(255) NOT NULL,
  `Description` longtext NOT NULL,
  `TicketDescription` varchar(100) DEFAULT NULL,
  `Keywords` varchar(50) NOT NULL DEFAULT 'Please Fill',
  `SelfHelp` varchar(255) DEFAULT NULL,
  `Tier1` int(4) NOT NULL DEFAULT 56,
  `Tier2` int(4) DEFAULT NULL,
  `Tier3` int(4) DEFAULT NULL,
  `PasswordUsed` varchar(255) DEFAULT NULL,
  `INMultiple` longtext DEFAULT NULL,
  `URL` varchar(255) DEFAULT NULL,
  `CI` varchar(255) DEFAULT NULL,
  `IncidentTroubleshooting` longtext DEFAULT NULL,
  `AR_Approvals` varchar(255) DEFAULT NULL,
  `AR_Prerequisites` varchar(255) DEFAULT NULL,
  `AR_Workflow` longtext DEFAULT NULL,
  `BusinessOwner` varchar(255) DEFAULT NULL,
  `Stakeholders` varchar(255) DEFAULT NULL,
  `BusinessHours` varchar(255) DEFAULT NULL,
  `SupportHours` varchar(255) DEFAULT NULL,
  `Attachment_ID` int(4) DEFAULT NULL,
  `archived` tinyint(1) NOT NULL DEFAULT 0,
  `LastModBy` varchar(45) DEFAULT NULL,
  `LastModDate` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `ID` int(10) UNSIGNED NOT NULL,
  `MemberID` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `PageBackground` varchar(7) NOT NULL DEFAULT '#ddddff',
  `TitleColor` varchar(7) NOT NULL DEFAULT '#009900',
  `LinkSystem` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `MenuColor` varchar(7) NOT NULL DEFAULT '#fff4bb',
  `MouseOver` varchar(7) NOT NULL DEFAULT '#9999ff',
  `JQuery` varchar(100) NOT NULL DEFAULT 'ui-lightness',
  `DateEntered` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


--
-- Table structure for table `siteaccess`
--

CREATE TABLE `siteaccess` (
  `ID` int(1) UNSIGNED NOT NULL,
  `AccessLevel` varchar(45) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `siteaccess`
--

INSERT INTO `siteaccess` (`ID`, `AccessLevel`) VALUES
(0, 'No Access'),
(1, 'Has Access'),
(2, 'Help Desk Member'),
(8, 'QR Administrator'),
(9, 'Full Administrator'),
(9999, 'root');

-- --------------------------------------------------------

--
-- Table structure for table `workgroups`
--

CREATE TABLE `workgroups` (
  `ID` int(4) NOT NULL,
  `Name` varchar(75) DEFAULT NULL,
  `SearchCode` varchar(50) DEFAULT NULL,
  `EmailAddress1` varchar(100) DEFAULT NULL,
  `EmailAddress2` varchar(100) DEFAULT NULL,
  `EmailAddress3` varchar(100) DEFAULT NULL,
  `OncallPhone` varchar(14) DEFAULT NULL,
  `archived` tinyint(1) NOT NULL DEFAULT 0,
  `LastModBy` varchar(45) DEFAULT NULL,
  `LastModDate` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `workgroups`
--

INSERT INTO `workgroups` (`ID`, `Name`, `SearchCode`, `EmailAddress1`, `EmailAddress2`, `EmailAddress3`, `OncallPhone`, `archived`, `LastModBy`, `LastModDate`) VALUES
(1, 'Operations Team', 'OPERATIONS-TEAM', '', '', '', '', 0, NULL, '0000-00-00 00:00:00'),
(2, 'Development Team', 'DEVELOPMENT-TEAM', '', '', '', '', 0, NULL, '0000-00-00 00:00:00'),
(3, 'test workgroup', 'TESTWORK', 'test@example.com', 'helpdesk@example.com', NULL, '555-555-5555', 0, NULL, '0000-00-00 00:00:00'),
(4, 'test workgroup', 'TESTWORK', 'test@example.com', 'helpdesk@example.com', NULL, '555-555-5555', 0, NULL, '0000-00-00 00:00:00'),
(5, 'test workgroup', 'TESTWORK', 'test@example.com', 'helpdesk@example.com', NULL, '555-555-5555', 0, NULL, '0000-00-00 00:00:00'),
(6, 'test update workgroup', 'TESTUPDATE', 'test@example.com', NULL, NULL, '403-555-helpdesk', 1, NULL, '0000-00-00 00:00:00'),
(7, 'brad', NULL, 'brad@thebtm.ca', NULL, NULL, '555-555-5555', 1, 'root', '2011-08-15 15:21:24'),
(8, 'Brad', NULL, 'Brad', NULL, NULL, NULL, 1, 'root', '2011-08-15 15:21:32'),
(9, 'Brad', NULL, 'Brad', 'Brad', 'Brad', 'Brad', 0, NULL, '0000-00-00 00:00:00'),
(10, 'testing php', 'testingphp', 'testing@example.com', 'testing@example.com', 'testing@example.com', 'stuff', 0, NULL, '0000-00-00 00:00:00'),
(11, 'test workgroup', 'TESTING', 'test@example.com', 'helpdesk@example.com', NULL, '555-555-5555', 0, NULL, '2011-08-15 05:46:07'),
(12, 'test workgroup', 'TESTING', 'test@example.com', 'helpdesk@example.com', NULL, '555-555-5555', 0, NULL, '2011-08-15 05:46:09'),
(13, 'test workgroup', 'TESTING', 'test@example.com', 'helpdesk@example.com', NULL, '555-555-5555', 0, NULL, '2011-08-15 05:46:10'),
(14, 'vbnm', 'cvbnm', 'vbnm', NULL, NULL, 'vbnm', 1, 'root', '2011-08-15 15:29:19'),
(15, 'vbnm', 'b nm,', NULL, NULL, NULL, NULL, 0, 'root', '2011-08-15 08:01:34'),
(16, 'test workgroup', 'TESTING', 'test@example.com', 'helpdesk@example.com', NULL, '555-555-5555', 0, NULL, '2011-08-15 08:02:53'),
(17, 'vbnm,', 'vgbhnm', NULL, NULL, NULL, NULL, 1, 'root', '2011-08-15 15:29:26'),
(18, 'bnmvbnm', NULL, 'bnm,', NULL, NULL, NULL, 1, 'root', '2011-08-15 15:21:12'),
(19, 'this is a test', 'THISISATEST', NULL, NULL, NULL, NULL, 0, 'brad', '2013-04-10 05:25:12'),
(20, 'dfghjk', 'tyuio', 'fghjk', 'fghjk', NULL, NULL, 1, 'root', '2011-08-18 06:04:46'),
(21, 'testing', 'testing', 'sdfghjkl', NULL, NULL, NULL, 0, 'root', '2011-08-19 15:15:45');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `attactments`
--
ALTER TABLE `attactments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bugreport`
--
ALTER TABLE `bugreport`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `members`
--
ALTER TABLE `members`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `quickreference`
--
ALTER TABLE `quickreference`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `siteaccess`
--
ALTER TABLE `siteaccess`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `workgroups`
--
ALTER TABLE `workgroups`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `ID` (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `attactments`
--
ALTER TABLE `attactments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `bugreport`
--
ALTER TABLE `bugreport`
  MODIFY `ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `members`
--
ALTER TABLE `members`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `quickreference`
--
ALTER TABLE `quickreference`
  MODIFY `ID` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=612;
--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;
--
-- AUTO_INCREMENT for table `workgroups`
--
ALTER TABLE `workgroups`
  MODIFY `ID` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=112;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
