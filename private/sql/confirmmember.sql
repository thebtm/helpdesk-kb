DELIMITER $$

DROP FUNCTION IF EXISTS `ConfirmMember` $$

CREATE FUNCTION `ConfirmMember`(
    check_Username varchar(45),
    check_Password varchar(255)) RETURNS int(11)
BEGIN
    DECLARE intcheckId INTEGER;
    DECLARE intAccessLevel INTEGER DEFAULT 0;

    SELECT id INTO intCheckId FROM members WHERE Username=Check_Username;

    IF (intCheckId=0) THEN
        #confirms Root was given the correct password
        SELECT AccessLevel INTO intAccessLevel FROM members WHERE passwrd=check_Password;

        IF (intAccessLevel > 0) THEN
            Update members
            Set LastSignIn=CURRENT_TIMESTAMP
            Where ID=intCheckId;
        END IF;

    ELSE

        IF (intCheckId>0) THEN
            #updates the users last login and gets their acesss level
            Update members
            Set LastSignIn=CURRENT_TIMESTAMP
            Where ID=intCheckId;
            SELECT AccessLevel INTO intAccessLevel FROM members WHERE id=intCheckId;

        ELSE
            #Adds user to the DB
            Insert into members
            (ID, Username, AccessLevel, Passwrd, LastSignIn)
            values
            (null, check_Username, 0, null, CURRENT_TIMESTAMP);
        END IF;

    END IF;

    RETURN(intAccessLevel);

END $$

DELIMITER ;