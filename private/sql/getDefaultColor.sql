DELIMITER $$

DROP PROCEDURE IF EXISTS `getDefaultColors` $$

CREATE PROCEDURE `getDefaultColors` ()
BEGIN
    Select * from settings where id=0;
END $$

DELIMITER ;