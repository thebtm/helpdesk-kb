DELIMITER $$

DROP PROCEDURE IF EXISTS `insert_wg` $$

CREATE PROCEDURE `insert_wg` (
    in_name varchar(75),
    in_searchcode varchar(50),
    in_email1 varchar(100),
    in_email2 varchar(100),
    in_email3 varchar(100),
    in_phone varchar(14),
    in_moduser varchar(50)
)
BEGIN
    insert into workgroups
    (ID, Name, SearchCode, EmailAddress1, EmailAddress2, EmailAddress3, OncallPhone, archived, LastModBy, LastModDate)
    values
    (null, in_name, in_searchcode, in_email1, in_email2, in_email3, in_phone, 0, in_moduser, current_timestamp);
    select id from workgroups order by id desc limit 0,1;
END $$

DELIMITER ;