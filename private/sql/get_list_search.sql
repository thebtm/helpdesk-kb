DELIMITER $$

DROP PROCEDURE IF EXISTS `qr_list_search` $$

CREATE PROCEDURE `qr_list_search`(
    list_search varchar(100),
    list_archived boolean)
BEGIN

    IF list_archived THEN
        SELECT `quickreference`.`ID`, `quickreference`.`Name`, `quickreference`.`Description`, `workgroups`.`ID` as wg_id, `workgroups`.`Name` as wg_name, `quickreference`.`AR_Approvals`, `quickreference`.`URL`
        FROM `workgroups`
                 Left JOIN `quickreference` ON `workgroups`.`ID` = `quickreference`.`Tier1`
        WHERE ((`quickreference`.`Name` like list_search)
            OR (`quickreference`.`Description` like list_search)
            OR (`quickreference`.`Keywords` like list_search))
        ORDER BY `quickreference`.`ID` ASC;
    ELSE
        SELECT `quickreference`.`ID`, `quickreference`.`Name`, `quickreference`.`Description`, `workgroups`.`ID` as wg_id, `workgroups`.`Name` as wg_name, `quickreference`.`AR_Approvals`, `quickreference`.`URL`
        FROM `workgroups`
                 LEFT JOIN `quickreference` ON `workgroups`.`ID` = `quickreference`.`Tier1`
        WHERE ((`quickreference`.`Name` like list_search)
            OR (`quickreference`.`Description` like list_search)
            OR (`quickreference`.`Keywords` like list_search))
          AND `quickreference`.`Archived` = 0
        ORDER BY `quickreference`.`ID` ASC;
    END IF;
END $$

DELIMITER ;