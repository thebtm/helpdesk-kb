DELIMITER $$

DROP PROCEDURE IF EXISTS `get_members_access` $$

CREATE PROCEDURE `get_members_access` ()
BEGIN
    SELECT `members`.`ID` as m_id, `members`.`Username`, `members`.`LastSignIn`, `siteaccess`.`ID` as sa_id, `siteaccess`.`AccessLevel`
    FROM `siteaccess`
             LEFT JOIN `qrgdev`.`members` ON `siteaccess`.`ID` = `members`.`AccessLevel`
    Where members.Username <> ""
    ORDER BY `members`.`ID` ASC;
END $$

DELIMITER ;