<?php
/**
 * @author Brad Mouck
 * @copyright 2011
 *
 * This page is to declare my Constants all in one location so all my php
 * pages can have the same set of Constants that i declare.
 */


//check to see if the constants are loaded
define("LOADED_CONSTANTS", true);

//sets the type of change being completed
define("QRG" , "qrg", false);
define("WG", "wg", false);

//action being completed
define("MODIFY_ENTRY", 1);
define("NEW_ENTRY", 0);
define("SAVE_ENTRY", 3);
define("AJAX_CALLED", "true");

//access level checks and root account
define("ROOT", "root");
define("QR_ADMIN", 8);
define("FULL_ADMIN", 9);
define("QR_ACCESS", 1);
define("ROOT_ACCESS", 9999);

//file paths for various items
//define("CLASS_WRITELOG", "writelog.php");
define("PRIVATE_PATH", dirname(__FILE__));
define("PROJECT_PATH", dirname(PRIVATE_PATH));
define("PUBLIC_PATH", PROJECT_PATH . '/public');
//define("SAVE_PATH", PRIVATE_PATH . "/logs/");
define("LOG_PATH", PRIVATE_PATH . "/logs/");
define("OLD_MENU", 1);
define("LIST_MENU", 2);
define("HTML_BLACK", "#000000");

// set to true if you want to authenticate with an Active Directory server and set server;
define("AD_CHECK", false); // should be stored in a database or config file able to be edited by the site.
define("AD_SERVER", "example.ad");

$modUser = $_SESSION['username'];
if(!isset($_SESSION['ulColor']))
{
    $_SESSION['ulColor'] = "#fff4bb";
    $_SESSION['liColor'] = "#9999ff";
}

$_SESSION['adminAccess'] = $_SESSION['auth'] >= QR_ADMIN;
$_SESSION['devAccess'] = $_SESSION['auth'] >= FULL_ADMIN;

?>