<?php

/**
 * @author Brad Mouck
 * @copyright 2011
 */
/**
 * ticketData()
 * 
 * we use this function to generate the div tag information for each entry retured from the database.
 * giving each div a id based on the id given back from the table quickreference
 * 
 * $id = the table ID.
 * $name = information in the name field on the table.
 * $description = information in the descrition field in the table
 * $approval = the data from the approval field of the table
 * $url = is the documented URL
 * $teirID (optional) = sets up the link to so the workgroup details from the workgroup table using
 *                      the information in the Tier data from the selected information.
 * 
 *  returns the list time and div tag to maintain consistancy.
 * 
 * @param mixed $id
 * @param mixed $name
 * @param mixed $tier1
 * @param mixed $description
 * @param mixed $approval
 * @param mixed $url
 * @param mixed $tierID
 * @return returns the list time and div tag to maintain consistancy.
 */
function ticketData($id, $name, $tier1, $description, $approval, $url, $tierID = null)
{
    $output = "<a href='javascript:DivShow(\"div" . $id . "\")'>" . $name . "</a>" . " - " . $tier1 . "<br />\n";
    $output .= "<div id='div" . $id . "' class='summery'>\n";
    $output .= " <table class='summery'>\n";
    $output .= "  <tr><td>Name:</td><td>$name</td><td>Teir 1:</td><td>";
    if($tierID != "" && $tierID != null)
    {
        $output .= "<a href='javascript:showDetailsAjax($tierID, \"dTeam\", \"" . WG . "\");'>$tier1</td></tr>\n";
    }
    else
    {
        $output .= "$tier1</td></tr>\n";
    }
    $output .= "  <tr><td>URL:</td><td>$url</td><td>Approval:</td><td>$approval</td></tr>\n";
    $output .= "  <tr><td>Description:</td><td colspan='3'>$description</td></tr>\n";
    $output .= " </table>\n";
    $output .= " <a href='javascript:showDetailsAjax($id, \"dDetails\", \"" . QRG . "\");'>Details<a><br />\n";
    $output .= " <br />\n";
    $output .= "</div>\n"; 
    return $output;
}

function setDataForSQL($value)
{
    $return = "null";
    if($value != null)
    {
        $return = "'$value'";
    }
    //echo $value . " - " . $return;
    return $return;
}

function getDir($dir)
{
    if (is_dir($dir)) 
    {
        $output = "<table>";
        if ($dh = opendir($dir)) 
        {
            while (($file = readdir($dh)) !== false) 
            {
                $output .= "<tr>";
                if(filetype($dir . $file) == 'file')
                {
                    $output .= "<td>filename: <a href='javascript:getPage(\"getLogs.php\", \"fileData\", \"file=" . $dir . $file . "\");' >$file</a> </td><td> filetype: " . filetype($dir . $file) . "</td>\n";
                }
                else
                {
                    $output .= "<td>filename: $file </td><td> filetype: " . filetype($dir . $file) . "</td>\n";
                }
                $output .= "</tr>";
            }
            closedir($dh);
        }
        $output .= "</table><div id='fileData'></div>";
    }
    
    return $output;
}

function confirmAccess($username, $log, $mysqli)
{
    $sqlCall = "Select ConfirmMember('$username', 'null')";
    $log->write($sqlCall);
    $result = $mysqli->query($sqlCall);
    if($mysqli->error)
    {
        echo $mysqli->error;
        $log->write($mysqli->error);
        die();
    }
    $row = $result->fetch_array(MYSQLI_NUM);
    return $row[0];
}
?>