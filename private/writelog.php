<?php

/**
 * @author Brad Mouck
 * @copyright 2011
 */

class WriteLog
{
    private $savePath;
    private $fileName;
    private $file;
    
    function __construct($savePath, $fileName)
    {
        $this->openFile($savePath, $fileName);
    }
    
    function __destruct()
    {
        $this->closeFile();
    }
    
    function changeFile($fileName)
    {
        $this->closeFile();
        $this->openFile($this->savePath, $fileName);
    }
    
    function getFileName()
    {
        return $this->fileName;
    }   
    
    function getSavePath()
    {
        return $this->savePath;
    }
    
    function write($strData)
    {
        fwrite($this->file, $strData);
        fwrite($this->file, "\n");
    }
    
    private function closeFile()
    {
        $strFinal = "";
        
        for($i = 0; $i <= 40; $i++)
        {
            $strFinal .= "-";
        }
        
        fwrite($this->file, $strFinal);    
        fwrite($this->file, "\n");    
        fclose($this->file);
    }
    
    private function openFile($savePath, $fileName)
    {
        $dateTime = time();
        $date = date("Y-M-d H:i:s", $dateTime);
        
        $serverData = "Broswer: " . $_SERVER['HTTP_USER_AGENT'] . "\n";
        $serverData .= "IP Address: " . $_SERVER['REMOTE_ADDR'] . "\n";
        
        $this->savePath = $savePath;
        $this->fileName = $fileName;
        $this->file = fopen($savePath . $fileName, "a");
        
        
        fwrite($this->file, "Date: " . $date . "\n");
        fwrite($this->file, $serverData);
    }
}

?>